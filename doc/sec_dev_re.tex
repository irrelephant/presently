\section{РАЗРАБОТКА ПРОГРАММНЫХ МОДУЛЕЙ}
\label{sec:dev}

При разработке системы одними из наиважнейших требований к исходному коду являются его расширяемость и поддерживаемость. Реализация программных модулей с учетом этих требований приводит к простоте расширения функционала в критических местах, обеспечению разделенности и независимости компонентов системы, что улучшает их тестируемость и в целом позволяет добиться реализации более стабильной и простой в понимании кодовой базы. Для обеспечения выделения из цельного объекта только нужного в некотором контексте фрагмента логики используются механизмы смешений (Mixin) и приведение типов к типу желаемого интерфейса. Последний метод возможен благодаря системе типизации, проверяемой на этапе компиляции языком TypeScript. Далее в разделе в деталях будут рассмотрены механизмы, обеспечивающие взаимодействие различных программных модулей и расширяемость системы в целом.

Внешний вид разрабатываемых модулей и интерфейсы, связывающие их с пользовательским интерфейсом и main-модулем представлены на диаграмме классов на чертеже ГУИР.400201.073 РР.1.

\subsection{Модуль Core}
\label{sec:dev:core}

Модуль \texttt{Core}, будучи по большей части пространством имен всевозможных объектов-хранилищ данных о презентации, использует большую часть интерфейсов, предоставляемых другими компонентами и модулями, среди которых можно отметить следующие интерфейсы:

\begin{enumerate}[label=\arabic*.]
\item{\texttt{IRenderable} -- интерфейс, предоставляемый модулем \texttt{Rende-ring}, указывающий на то, что объект реализует набор правил по отрисовке его представления на полотне.}
\item{\texttt{IUIDisplayable} -- интерфейс, предоставляемый модулем \texttt{UI}, указывающий на то, что объект можно отобразить на пользовательском интерфейсе.}
\item{\texttt{IPersistable} -- интерфейс-маркер, предоставляемый модулем \texttt{Persistence}, указывающий на то, что объекту известно о типе сериализатора (\texttt{IPersister<T>}), используемом для сохранения его в JSON-объект.}
\end{enumerate}

\subsubsection{Интерфейс IRenderable}

Интерфейс \texttt{IRenderable} содержит метод, определяющий порядок отрисовки элемента и дочерних ему элементов на полотне презентации. Все абстракции, объявленные в модуле \texttt{Core}, реализуют этот интерфейс, однако явным образом он используется лишь в классе \texttt{CanvasRenderer} для того, чтобы отобразить объект презентации. Логика отрисовки презентации слишком громоздка и сложна для того, чтобы хранить порядок отрисовки элементов внутри самого объекта презентации, поэтому она реализована в серии методов \texttt{CanvasRenderer}, который в свою очередь, отображает все содержимое презентации вызывая метод, объявленный в интерфейсе. Сам интерфейс представлен на следующем листинге:

\begin{lstlisting}[language=TypeScript, label=lst:dev:irenderable]
export interface IRenderable {
    // Render the target object onto the canvas.
    // ctx - Context to render in
    // viewportState -- Camera info
    render(ctx: CanvasRenderingContext2D, viewportState: ViewportState): void;
}
\end{lstlisting}

Пример реализации данного интерфейса можно обнаружить, например, в типе \texttt{Frame}, реализующим логику отрисовки кадра и его содержимого с учетом текущего режима отображения:

\begin{lstlisting}[language=TypeScript, label=lst:dev:irenderableimpl]
public render(ctx: CanvasRenderingContext2D,
              viewportState: ViewportState): void {
    let zoomFactor = viewportState.zoom / this.targetViewportState.zoom;

    // Takes current mode into account
    this.setStrokeStyleForFrameOutline(ctx);

    ctx.strokeRect((this.targetViewportState.xPos - viewportState.xPos) * viewportState.zoom,
                   (this.targetViewportState.yPos - viewportState.yPos) * viewportState.zoom,
                    ctx.canvas.width * zoomFactor,
                    ctx.canvas.height * zoomFactor);

    this.frameElements.forEach(element: IRenderable => element.render(ctx, viewportState));
}

\end{lstlisting}

\subsubsection{Интерфейс IUIDisplayable}
Интерфейс, необходимый объектам модуля \texttt{Core} для сигнализации о наличии в них методов, позволяющих получить из них модели отображения для пользовательского интерфейса. Объекты модуля для сохранения принципа <<Разделения ответственностей>> не должны знать о деталях отображения интерфейса, не должны иметь завязок на события либо идентификаторы элементов пользовательского интерфейса, тем не менее в системе существут ряд абстракций, чье прямое назначение состоит в том, чтобы отображать данные объекты и обновлять их по мере внесения пользователем изменений. В соответствии с концепцией MVVM, данные сущности носят название <<Моделей отображения>>, установление соответствия с которыми -- и есть основное назначение данного интерфейса. Его исходный код содержит лишь один метод:

\begin{lstlisting}[language=TypeScript, label=lst:dev:iuidisplayable]
export interface IUIDisplayable {
    // Build the domain object's viewmodel
    getViewModel(): any;
}
\end{lstlisting}

Как видно из исходного кода, интерфейс не устанавливает четкой типовой зависимости, так как объектом модели отображения может быть даже простой словарь, однако такого, как правило, не случается потому как модели отображения содержат методы для доступа к данным, хранящимся в оригинальной модели. Примером реализации данного интерфейса может послужить тип \texttt{HtmlFrameElement}. Следует отметить, что данный интерфейс служит по своей сути преобразователем типов: из типа доменного объекта в объекту модели отображения.

\begin{lstlisting}[language=TypeScript, label=lst:dev:iuidisplayableimpl]
public getViewModel(): any {
    return new HtmlElementViewModel(this);
}
\end{lstlisting}

\subsubsection{Интерфейс IPersistable}

Интерфейс, указывающий на то, что реализующий его тип способен создать экземпляр типа-сериализатора, способного сохранить его в JSON-представление и наоборот. Все доменные типы реализуют данный интерфейс для сохранения своих данных на жесткий диск. Интерфейс представлен на следующем листинге:

\begin{lstlisting}[language=TypeScript, label=lst:dev:ipersistable]
export interface IPersistable {
    // Get the object's persister
    getPersister<T>(): IPersister<T>;
}
\end{lstlisting}

Как видно из листинга, интерфейс имеет привязку по типу, так что формально возможен сценарий, когда объект <<знает>> о том, как собрать сериализатор для объекта другого типа, однако на практике такая ситуация не используется. Так же как и предыдущий интерфейс, данный является маппером, устанавливающим связь между двумя типами различных модулей, но описывающих одну и ту же абстракцию с разных сторон. Примером использования данного интерфейса может послужить объект презентации, реализующий данный интерфейс. Все доменные объекты системы имеют информацию о том, какой тип сериализатора им подходит, и эта информация используется другими сериализаторами при рекурсивном сохранении объектов в JSON-представления.

\begin{lstlisting}[language=TypeScript, label=lst:dev:ipersistableimpl]
public getPersister<Presentation>():
       IPersister<Presentation> {

    return new PresentationPersister();
}
\end{lstlisting}

Данный интерфейс предназначен для создания экземпляра сериализатора, хотя сами сериализаторы используют его чтобы получить экземпляр сериализатора дочернего элемента. Такой вызов можно наблюдать в сериализаторе объекта презентации:

\begin{lstlisting}[language=TypeScript, label=lst:dev:ipersistableimpl]
public asJsonObject(persistable: Core.Presentation): any {
    return {
        backgroundUri: persistable.backgroundUri,
        frames: persistable.frames.map((child: IPersistable) => {
            // Call to IPersistable method
            return child
              .getPersister()
              .asJsonObject(child))
        }
    };
}
\end{lstlisting}

Метод \texttt{getPersister()} можно считать методом-фабрикой, позволяющим избежать излишнего использования реального типа объекта для определения его функциональности, что позволяет сделать систему более тестирумой (там, где система принимает аргумент, реализующий интерфейс, его можно подменить на mock-объект, как это описано в разделе \ref{sec:testing}) а так же отдалить различные ее компоненты друг от друга.

\subsubsection{Расширяемость FrameElement и FramePersister}

Типы \texttt{FrameElement} и \texttt{FramePersister} требуют особого внимания так как используют необычный способ инициализации для повышения расширяемости. Поскольку одним из потенциальных (и наиболее вероятных) направлений расширения системы является добавление новых типов элементов, то в процессе разработки необходимо было реализовать способ, позволяющий расширять перечень типов элементов не прибегая к изменению существующего кода (в частности кода моделей отображения для пользовательского интерфейса, кода сериализации презентации, кода отображения презентации). Как известно, TypeScript не обладает богатым функционалом в области рефлексии, однако ввиду некоторых особенностей языка, реализация подобных механизмов становится возможной. Среди таких механизмов можно отметить следующие:

\begin{itemize}
\item{любой класс TypeScript имеет тип \texttt{function} и является собственным конструктором;}
\item{любую функцию сигнатуры \texttt{(...): void => {}} можно использовать с оператором \texttt{new};}
\item{любые статические данные можно объявить членом функции конструктора;}
\item{статические данные можно переопределить в дочерних типах.}
\end{itemize}

Благодаря данным особенностям языка, можно реализовать библиотеку, возвращающую конструкторы для классов как простые функции, после чего возможно создание их экземпляров с помощью оператора \texttt{new}. В этом и заключается механизм повышения расширяемости данных типов. Сам класс \texttt{FrameElement} -- абстрактный, реализует минимальную логику по получению конструкторов различных дочерних типов:

\begin{lstlisting}[language=TypeScript, label=lst:dev:framelements1]
module Presently.Core {
    export class FrameElement {

        public static frameElementRegistry: {[typename: string] : any} = {};

        // Yields the constructor of the type by it's name
        public static byTypeName(typename: string): any {
            return FrameElement.frameElementRegistry[typename];
        }

        // returns the key name of the registered type
        public static getTypeName(instance: FrameElement) {
            for (let key in FrameElement.frameElementRegistry) {
                if (instance instanceof FrameElement.frameElementRegistry[key]) return key;
            }

            return null;
        }

        // returns list of all the known types
        public static getRegisteredTypes(): string[] {
            return Object.keys(FrameElement.frameElementRegistry);
        }

        // Members omitted for brevity ...
    }
}
\end{lstlisting}

Поскольку в паре типов \texttt{FrameElement} и \texttt{FrameElementPersi-ster} существует отношение зависимости, то схожую логику по получению названия типа можно не реализовывать в типе-сериализаторе, однако он реализует такую же статическую коллекцию конструкторов и механизм доступа к ней:

\begin{lstlisting}[language=TypeScript, label=lst:dev:framelements2]
module Presently.Persistance {
    export class FrameElementPersister {
        public static frameElementPersisterRegistry: {[typename: string] : any} = {};

        public static byTypeName(typename: string): any {
            return FrameElementPersister.frameElementPersisterRegistry[typename];
        }

        // Members omitted for brevity ...
    }
}
\end{lstlisting}

Данная инфраструктура, позволяющая хранить указатели на функции-конструкторы различных типов, позволяет произвести вызов данных конструкторов без знания реального пространства имен или названия типа элемента, оперируя лишь ключами-названиями его типа, стоит лишь провести его регистрацию. Регистрация элемента заключается в добавлении его конструктора в статический словарь, что может быть сделано непосредственно за пределом регистрирующего класса, необходимо лишь убедиться в том, что порядок компиляции таков, что типы \texttt{FrameElement} и \texttt{FrameElementPersister} компилируются как можно раньше. Для регистрации типов используется следующий фрагмент кода (на примере элемента типа html):

\begin{lstlisting}[language=TypeScript, label=lst:dev:framelements3]
module Presently.Core {
    export class HtmlElement extends FrameElement {
        // Done outside the class
    }

    Presently.Core.FrameElement.frameElementRegistry['html'] = Presently.Core.HtmlElement;
}
\end{lstlisting}

Схожую регистрацию проходят также типы-сериализаторы (также на примере типа html):

\begin{lstlisting}[language=TypeScript, label=lst:dev:framelements4]
Presently.Persistance.FrameElementPersister.frameElementPersisterRegistry['html'] = Presently.Persistance.HtmlElementPersister;
\end{lstlisting}

В дальнейшем, пользовательский интерфейс и прочая логика, реализующая отображение перечня типов элементов может использовать описанные выше статические методы для получения доступных и реализованных в системе типов. Так, например, реализована логика по отображению манипулятора для выбора типа элементов при добавлении на полотно:

\begin{lstlisting}[language=TypeScript, showstringspaces=false, label=lst:dev:framelements4]select#add-element-selector(
    // index.jade
    select(data-bind="options: availableElementTypes, \
               optionsCaption: 'Select Element', \
               optionsText: 'displayName', \
               value: selectedElementType")

    // AppViewModel.ts
    this.availableElementTypes = ko.observableArray(
        Core.FrameElement.getRegisteredTypes()
            .map((key) => { return {
                'typeName': key,
                'displayName': LocaleManager.translate(`elementType_${key}`)
            };
        })
    );
\end{lstlisting}

В случае добавления нового элемента и его регистрации, его значение автоматически появится на пользовательском интерфейсе поскольку тот не зависит от перечислений, указанных непосредственно в разметке пользовательского интерфейса.
Такой подход не позволяет расширить список элементов без перезагрузки приложения, так как списки типов элементов инициализируются статически, однако расширение списка элементов динамически потребовало бы переработки подсистемы загрузки и сохранения презентаций ввиду ее зависимости от словарей, описывающих типы элементов.

\subsection{Модуль Persistance}

Модуль \texttt{Persistance} -- модуль загрузки и сохранения доменных моделей системы, предназначенный для сохранения состояния презентаций и ее элементов между перезапусками приложения. Основная часть логики по сохранению ресурсов сосредоточена в статическом типе \texttt{ResourceLoader}, чьей основной задачей является сохранение содержимого удаленных ресурсов на жесткий диск и их чтение по требованию подсистемы отрисовки полотна. Поскольку операции загрузки и сохранения потенциально могут занимать продолжительное время, \texttt{ResourceLoader} загружает, конвертирует и сохраняет ресурсы на диск асинхронно с использованием механизма так называемых Promise-объектов (объектов-обещаний).

Механизм работы объекта обещания изображен на рисунке \ref{fig:dev:promise} и состоит в следующем:

\begin{enumerate}[label=\arabic*.]
\item{Объект <<потребитель>> вызывает <<сервис>> с целью получить некоторые данные либо вызвать удаленный API, ответ от которого может занять неопределенное время.}
\item{Сервис создает объект типа \texttt{Promise<T>}, где \texttt{T} -- тип ожидаемого потребителем значения (может быть \texttt{void}).}
\item{Конструктор типа \texttt{Promise<T>} принимает в качестве аргумента функцию сигнатуры \texttt{(resolve: (T) => void, reject: (any) => void) => void}. Внутри данной функции, сервис обращается к долгоработающему API (<<сторонний сервис>>) -- это может быть unmanaged-код, управляемый системой сигналов или другой сервис. Единственным требованием к долгоработающему API является наличие некоторго механзма оповещения о завершении операции. Например, вызов callback-функции или некоторого сигнала.)}
\item{После начала длительной операции и возвращения объекта типа \texttt{Promise<T>}, <<Потребитель>> продолжает свою работу, ожидая ответа от результата длительного вызова. Данные действия производятся синхронно старту длительной операции. Объект потребитель может делать другие долгие вызовы, обращаться к другим сервисам и т.д.}
\item{После того как все синхронные операции завершатся, поток, обрабатывающий поведение объекта потребителя завершит работать и выйдет за пределы области пользовательского кода, виртуальная машина языка JavaScript начнет ожидать очередного события в очереди. Рано или поздно, от <<стороннего сервиса>> придет ответ и будет вызвана callback-функция либо сработает сигнал, который будет воспринят <<сервисом>>.}
\item{Контроль будет передан callback-функции <<сервиса>>, которая либо вызовет функцию \texttt{resolve: (T) => void} с некоторым результатом операции в качестве аргумента, сигнализируя объекту-обещанию о том, что долгоработающая операция завершилась успешно и были получены данные типа T, либо функцию \texttt{reject: (any) => void}, сигнализирующая об ошибке. Во втором случае, может быть передан описатель ошибки любого типа и <<потребитель>> может обработать ее соответствующим образом.}
\item{Вызов \texttt{resolve} или \texttt{reject} вызывает обработчики, зарегистрированные объектом <<потребитель>>, тем самым возобновляя процесс выполнения программы.}
\end{enumerate}

Техника создания и использования объектов обещаний приведена на листинге ниже на примере длительной операции чтения содержимого удаленного ресурса, который был сохранен в файл на жестком диске и помещение этого содержимого в \texttt{Blob}-объект:
\begin{lstlisting}[language=TypeScript, showstringspaces=false, label=lst:dev:framelements4]select#add-element-selector(

private static buildImageBlob(src: string): Promise<HTMLImageElement> {
    // Create promise and pass it a (resolve, reject) => void
    return new Promise<HTMLImageElement>((resolve, reject) => {
        // Start a long running operation with a callback argument
        fs.readFile(this.getResourcePath(src), (err: any, contents: Buffer) => {

            // After the long-running operation is done, check the status.
            // Reject in case of error
            if (err) reject(err);

            // Transform the result and resolve the promise in case of success.
            let dataBlob = new Blob([contents], { type: 'image/png' });
            let img = new Image()
            img.src = URL.createObjectURL(dataBlob);
            resolve(img);
        });
    });
}

public static getImageResource(src: string): Promise<HTMLImageElement> {
    return new Promise<HTMLImageElement>((resolve, reject) => {
        if (!this.doesResourceExist(src)) {
            this.prefetchImage(src).then((resourcePath) => {
                this.buildImageBlob(src).then(resolve);
            });
        } else {
            this.buildImageBlob(src).then(resolve);
        }
    });
}
\end{lstlisting}

Как показано на листинге выше, операции можно объединять в цепочки и вызывать одну за другой после завершения, однако не блокируя при этом поток выполнения. Такой подход, когда одна операция возвращает обещание на результат, а другая возвращает обещание на обработанный результат непосредственно после получения результата первой (и так далее), получил название <<неблокирующее программирование>> и напоминает по своей сути реактивное программирование. Системы, которые построены таким образом, определяют очередь операций и порядок их завершения в основном (управляющем) потоке, тогда как сами операции выполняются в отдельных потоках или же совершенно другими процессорами в целом.

Следует отметить, однако, что виртуальная машина языка JavaScript выполняется в один поток и, соответственно, выполняет данную очередь в один поток. Виртуальная машина решает о том, какая асинхронная операция будет выполняться следующей либо выполняет ее обработку только в том случае, если пользовательский код достиг последней своей инструкции и вышел в область управления виртуальной машины. Только в этом случае, язык может начать прослушивать сигналы и обрабатывать приходящие извне события. Это означает, что несмотря на видимую сложность организации подобных систем, где обновление внутреннего состояния происходят асинхронно и каскадами, она полностью потокобезопасна.

Использование механизма Promise-обещаний позволяет избежать одного из наиболее серьезных антипаттернов программирования в среде node.js -- так называемый \textit{callback hell} -- антипаттерн, возникающий в системах, где используется слишком много асинхронных вызовов и их обработка производится посредством вызова callback-функций. Поддержка и расширение очереди команд в такой системе становится сложнее и сложнее с каждой добавленной callback-функцией не только из-за добавленной логики, но и потому что для цикличных вызовов очень сложно извлечь общую логику с целью использовать цикл. Механизм Promise-объектов позволяет лучше упорядочить очередь асинхронных вызовов а также реализует множество методов, позволяющих комбинировать Promise-объекты. Так, например, библиотека \texttt{\$q} реализует методы, позволяющие получить Promise-объект, комбинирующий коллекцию Promise-объектов коллекции завершили выполнение, либо когда завершил выполнение один из них. Библиотека \texttt{\$q} используется в процессе модульного тестирования модуля \texttt{Presently.Persistance}, описанного в разделе \ref{sec:testing} потому как данный модуль тесно связан с модулем \texttt{fs} из стандартной библиотеки node.js, реализующей асинхронный доступ к файловой системе компьютера и интерфейсы используемых компонентов и их mock-реализаций должны совпадать.

\begin{figure}[ht]
\centering
  \includegraphics[scale=0.75]{promise.png}
  \caption{Диаграмма последовательности работы Promise-объекта}
  \label{fig:dev:promise}
\end{figure}

Механизм объектов-обещаний также активно используется в методах загрузки и сохранения презентации. Блок-схема алгоритма загрузки презентации из файла представлена на чертеже ГУИР.400201.073 ПД.

\subsection {Автоматизация сборки приложения}

Ключевое отличие приложения, реализованного на данном технологическом стеке от приложения, написанного на компилируемом языке состоит в том, что процесс сборки приложения и упаковки его в исполняемый модуль значительно отличается от компилируемого приложения. Приложения, реализованные на базе node.js и Electron выполняются в виде npm модулей, которые используют менеджер пакетов npm для установки зависимостей и библиотек-утилит.
Поскольку в проекте используется ряд надстроек над элементами стека, такие как язык TypeScript в качестве основного языка программирования, язык jade для описания разметки пользовательского интерфейса, язык less для реализации каскадных таблиц стилей, то требуется автоматизация процесса сборки приложения в дистрибутив-пакет, иначе процесс разрабоки быстро превратится в постоянный вызов компиляторов и трансляторов вместо самой разработки.

Для автоматической сборки решения используется npm-модуль \texttt{gulp}. По своей сущности \texttt{gulp} напоминает утилиту make, поставляемую с большинством дистрибутивов Linux. Для описания процесса сборки решения необходимо реализовать так называемый gulpfile -- скрипт, написанный на JavaScript и использующий gulp-расширения для произведения трансформаций над исходными файлами.

В рамках данной работы использовался gulp-модуль, автоматически отслеживающий все *.ts файлы, транслирующий их в *.js и упаковывающий в дистрибутив-пакет. На листинге ниже приведен скрипт, автоматизирующий процесс сборки TypeScript:

\begin{lstlisting}[language=TypeScript, label=lst:dev:automation]
const gulp = require('gulp')
const ts = require('gulp-typescript')
const jade = require('gulp-jade')
const less = require('gulp-less')
const flatten = require('gulp-flatten')
const path = require('path')

const appConfig = require('./config.json')

gulp.task('ts', () => {
    gulp
        .src(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.tsScripts, '**/*.ts'))
        .pipe(ts({
            noImplicitAny: true,
            out: 'app.js',
            lib: '',
            noEmitOnError: true
        }))
        .js.pipe(gulp.dest(path.join(appConfig.solutionDists.distFolder, appConfig.solutionDists.javascript)))
})

gulp.task('watch', ['ts'], () => {
    gulp.watch(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.tsScripts, '**/*.ts'), ['ts'])
    gulp.watch('locale.json', ['jade', 'copy-startup'])
})

gulp.task('copy-startup', () => {
    console.log('Copying startup files: ' + '{config.json,locale.json,' + path.join(appConfig.solutionSources.sourcesFolder, 'index.js') + '}')
    gulp
        .src('{config.json,locale.json,' + path.join(appConfig.solutionSources.sourcesFolder, 'index.js') + '}')
        .pipe(flatten())
        .pipe(gulp.dest(appConfig.solutionDists.distFolder))
})
\end{lstlisting}

Скрипт, приведенный выше, автоматически отслеживает все *.ts файлы и перекомпилирует их по изменению любого из них. Помимо прочьего, gulp анализирует изменение содержимого точки входа приложения и файлов конфигурации и ресурсов и автоматически копирует их в дистрибутив-пакет в случае изменения.
