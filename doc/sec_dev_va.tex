\section{РАЗРАБОТКА ПРОГРАММНЫХ МОДУЛЕЙ}
\label{sec:dev}

При разработке системы одними из наиважнейших требований к исходному коду являются его расширяемость и поддерживаемость. Реализация программных модулей с учетом этих требований приводит к простоте расширения функционала в критических местах, обеспечению разделенности и независимости компонентов системы, что улучшает их тестируемость и в целом позволяет добиться реализации более стабильной и простой в понимании кодовой базы. Для обеспечения выделения из цельного объекта только нужного в некотором контексте фрагмента логики используются механизмы смешений (Mixin) и приведение типов к типу желаемого интерфейса. Последний метод возможен благодаря системе типизации, проверяемой на этапе компиляции языком TypeScript. Далее в разделе в деталях будут рассмотрены механизмы, обеспечивающие взаимодействие различных программных модулей и расширяемость системы в целом. В процессе зазработки программного модуля система приобрела вид, представленный на диаграмме классов в ГУИР.400201.011 РР.1.

\subsection{Модуль UI}
\label{sec:dev:ui}

Модуль UI отвечает за управление пользовательским интерфейсом системы и содержит ряд интерфейсов-контрактов и классов для взаимодействия с прочими компонентами с большой степенью абстракции. Модуль UI предоставляет внешним модулям интерфейс \texttt{IUIDisplayable}, объявляющий метод обязанный реализовать набор правил по установлению связи между доменной абстракцией системы и объектом, являющимся его моделью отображения. Листинг интерфейса представлен ниже:

\begin{lstlisting}[language=TypeScript, label=lst:dev:iuidisplayable]
export interface IUIDisplayable {
    // Build the domain object's viewmodel
    getViewModel(): any;
}
\end{lstlisting}

Модель отображения -- особая сущность системы, представляющая собой набор методов и полей, предназначенных для управления определенным компонентом пользовательского интерфейса и устанавливающая связь между отображением (в рамках данной системы -- jade-разметкой) и доменными данными (объектами \texttt{Presentation}, \texttt{Frame}, \texttt{FrameElement}) из компонента \texttt{Presently.Core}. В рамках данной системы используется MVVM-фреймворк Knockout, соответственно дальнейшие появнения будут подразумевать использование абстракций из данного фреймворка. Говоря в общем виде, модель отображения есть представление данных в виде кода, например, в случае приложения управления список, модель отображения представляла бы из себя сам список а также набор методов, предназначенных для манипуляции данным списком в рамках пользовательского интерфейса, например, так называемые CRUD операции (Create, Read, Update, Delete). У модели отображения нет концепта о том, какие бывают элементы пользовательского интерфейса (кнопки или поля ввода) или стили отображения. Подобная абстракция и отделение пользовательского интерфейса от логики, его обрабатывающей, позволяет логике оставаться простой и расширяемой.

В самом простом понимании моделью отображения может являться обычный объект TypeScript, однако такой вариант не подходит в рамках данного проекта ввиду того, что такой пользовательский интерфейс был бы слишком статичен. Для установления связи между отображением и моделью отображения существует тип \texttt{KnockoutObservable} (обозреваемое поле) и ряд так называемых атрибут-binding'ов (в дальнейшем <<связующих атрибутов>>).

Для установления связи между элементом интерфейса следует указать связующий атрибут на контрольный элемент пользовательского интерфейса а также создать соответствующее обозреваемое поле:
\begin{lstlisting}[language=TypeScript, label=lst:dev:samplevm]
// ViewModel.ts - added knockout typings
///<reference types="knockout"/>

// View Model class
export class SampleViewModel {

    // Observable initialization
    observableField: KnockoutObservable<any>;
    constructor() {
        this.observableField = ko.observable('sample observable value');
    }

}

ko.applyBindings(new SampleViewModel());

// index.jade
input(data-binding="value: observableField")
\end{lstlisting}

Подобная разметка создает двухстороннюю связь между html-элемен-том типа \texttt{input} и обозреваемым полем типа \texttt{any} -- \texttt{observableField}.

Фреймворк Knockout реализует набор связующих атрибутов, реализующих различные способы связи отображения и модели отображения. В рамках системы наиболее часто используются следующие:

\begin{itemize}
\item{\texttt{value: observable} -- реализует двухстороннюю связь типа <<значение>>, синхронизируя значения, указанные в пользовательском интерфейсе и внутри обозреваемого поля;}
\item{\texttt{text: observable} -- реализует одностороннюю связь типа <<значение>>, синхронизируя значение, отображаемое на пользовательском интерфейсе с тем, что указан в обозреваемом поле без возможности редактирования;}
\item{\texttt{click: callback} -- реализует односторонню связь типа <<событие>>, вызывая указанный метод на модели отображения по нажатию на указанный элемент пользовательского интерфейса;}
\item{\texttt{hover: callback} -- реализует одностороннюю связь типа <<событие>>, вызывая указанный метод на модели отображения по наведению мыши на указанный элемент пользовательского интерфейса;}
\item{\texttt{options: observableArray} -- реализует одностороннюю связь типа <<значение>>, синхронизируя набор элементов типа \texttt{option} внутри связанного элемента типа \texttt{select} со значениями, указанными внутри обозреваемого \texttt{observableArray};}
\item{\texttt{with: observable} -- реализует одностороннюю связь типа <<значение>>, сменяя контекст выполнения текущего связующего атрибута на значение, указанное внутри \texttt{with};}
\item{\texttt{foreach: observableArray} -- реализует одностороннюю связь типа <<значение>>, повторя дочерние элементы разметки для каждого объекта, находящегося внутри обозреваемого \texttt{observableArray} и связывая дочерние элементы с каждым из объектов;}
\item{\texttt{visible: expression} -- реализует одностороннюю связь типа <<атрибут>>, пряча либо отображая связанный элемент пользовательского интерфейса в зависимости от значения, в которое вычислено выражение \texttt{expression};}
\item{\texttt{visible: (css-string) => expression} -- реализует одностороннюю связь типа <<атрибут>>, устанавливая либо удаляя класс \texttt{css-string} со связанного элемента в зависимости от значения, в которое вычислено выражение \texttt{expression}.}
\end{itemize}


В рамках системы существует ряд иерархически зависимых моделей отображения, основная из которых имеет название \texttt{AppViewModel} -- модель отображения, контролирующая глобальное состояние пользовательского интерфейса. Ряд зависимых моделей отображения, отвечающих за отображение таких элементов как \texttt{Frame}, \texttt{FrameElement} в свою очередь являются дочерними к ней и создают свои экземпляры с использованием метода, который объявлен в \texttt{IUIDisplayable}.

Основным потребителем объектов интерфейса \texttt{IUIDisplayable} является главная модель отображени приложения, контролирующая все состояние пользовательского интерфейса -- \texttt{AppViewModel}. Она использует данный интерфейс для регистрации в пользовательском интерфейсе новых доменных объектов, создаваемых в процессе работы приложения. Например, функция \texttt{placeElement} -- обрабочик события нажания на кнопку добавления элемента -- использует данный интерфейс следующим образом:

\begin{lstlisting}[language=TypeScript, label=lst:dev:placeelement]
public placeElement(elementTypeName: string): void {
    let elementType = Core.FrameElement.byTypeName(elementTypeName);
    if (elementType) {
        let element = new elementType(100, 100);
        this.frame.addElement(element);
        let displayable = element as IUIDisplayable;
        this.frameElements.push(element.getViewModel());
    }
}
\end{lstlisting}

Как видно из листинга, в коде не встречаются явно типизированные переменные, указывающий на истинный тип элемента. Поскольку вся работа с элементами проходит в подобной абстрактной среде, пользовательский интерфейс не может знать наверняка тип отображаемого элемента. Это приводит к тому, что при отображении полей метаданных на интерфейсе появляется необходимость динамически выстраивать разметку для отображения этих полей. Данный механизм предусматривает наличие следующей инфраструктуры в базовом типе \texttt{FrameElementViewModel}:

\begin{lstlisting}[language=TypeScript, label=lst:dev:metadatapanel1]
// ExtraField.ts
module Presently.UI {
    class ExtraField {
        displayName: string;
        private innerValue: any;
        value: KnockoutPureComputed<any>;

        constructor(public name: string, private domainObject: any) {
            this.displayName = LocaleManager.translate(`field_${name}`);
            this.value = ko.pureComputed({
                read: () => this.innerValue;
                write: (value: any) => {
                    this.innerValue = value;
                    this.updateDomainObject();
                }
            )
            this.initFromDomainObject();
        }

        public updateDomainObject() {
            this.domainObject[this.name] = this.innerValue;
        }

        public initFromDomainObject() {
            this.innerValue = this.domainObject[this.name];
        }
    }
}

// FrameViewModel.ts
module Presently.UI {
    export class FrameElementViewModel {
        public extraFields: KnockoutObservableArray<ExtraField>;

        constructor(domainObject: Core.FrameElement) {
            this.extraFields = ko.observableArray([]);
        }

        // .. Remainder omitted for brivety
    }
}
\end{lstlisting}

Поле \texttt{extraFields} указывает модели отображения на набор дополнительных редактируемых данных для определенного типа модели отображения. Например, тип \texttt{HtmlElementViewModel} реализует следующий перечень полей для отображения:

\begin{lstlisting}[language=TypeScript, label=lst:dev:metadatapanel2]
constructor(domainObject) {
    this.extraFields = ko.observableArray([
        new ExtraField('htmlContent', this.domainObject),
        new ExtraField('margin', this.domainObject)
    ]);
}
\end{lstlisting}

Данная конструкция позволяет создать два дополнительных поля на панели метаданных: Изменение данных полей обновляет соответствующие поля в доменном объекте а также в модели отображения.

jade-шаблон для отображения данного перечня полей выглядит следующим образом:
\begin{lstlisting}[language=TypeScript, label=lst:dev:metadatapanel3]
  .elements(data-bind="foreach: frameElements")
    .frame(data-bind="text: displayName")
      .frame-element-meta
        label | locale.xPos
        input.position(data-bind="value: xPos")
        label | locale.yPos
        input.position(data-bind="value: yPos")
        label | locale.xSize
        input.size(data-bind="value: xSize")
        label | locale.ySize
        input.size(data-bind="value: ySize")
      .frame-element-extra-meta(data-bind="foreach: extraFields")
          .group-container
            label(data-bind="text: displayName")
            input.extra-field(data-bind="value: value")
\end{lstlisting}

Секция связывания данных для дополнительных полей находится под классом \texttt{frame-element-extra-meta} и связывается двунаправленной связью с вычисляемым полем \texttt{value}, находящимся в коллекции \texttt{extra-Fields}. Его обновление приводит к вызову функции, зарегистрированной как обработчик события \texttt{write} в вычисленном свойстве. Такая двунаправленная связь позволяет не только обновить внутреннее значение модели отображения, но также и значение доменного объекта. Изображение сгенерированной разметки преведено на рисунке \ref{fig:dev:core:metadata-panel}

\begin{figure}[ht]
\centering
  \includegraphics[scale=1.3]{metadata_panel.png}
  \caption{Изображение панели метаданных HTML-элемента}
  \label{fig:dev:core:metadata-panel}
\end{figure}

\subsubsection{Класс CanvasInputManager}

Большая часть логики модуля \texttt{Presently.UI} сосредоточена в моделях отображения, однако та часть методов, которая описывает взаимодействие пользователя с полотном системы не находится внутри модели отображения. Поскольку все элементы, отображаемые внутри полотна, реально не отображены в HTML элементы в DOM-дереве, использование классического подхода Javascript-событий или паттерна MVVM, реализуемого фреймворком Knockout, для них невозможно: они просто не существуют в виде, пригодном для использования этих подходов. Все манипуляции пользователя непосредственно с полотном приложения реализованы на уровне событий, привязанных к HTML-документу. После того как данные события (такие как \texttt{mousemove, scroll, mousewheel, click} и прочие) срабатывают, они будут перехвачены обработчиками, зарегистрированными в типе \texttt{UI.CanvasInputManager} и обработаны в соответствии с текущим состоянием экземпляра \texttt{AppViewModel}. Детальное описание данных событий представлено в следующем списке:

\begin{enumerate}[label=\arabic*.]
\item{\texttt{mousewheel} -- производит обработку параметра приближения камеры, изменяя поле \texttt{zoom} текущего экземпляра \texttt{ViewportState} от 0.1 до 100. Данный коэффициент в дальнейшем используется для определения положения <<камеры>> презентации, а именно отдаления ее от полотна.}
\item{\texttt{dblclick} -- производит захват элемента полотна для перемещения его по полотну вместе с мышью в режиме редактирования. Поиск элемента производится путем рекурсивного обхода дерева элементов презентации с определением сперва кадров, а затем и элементов, на которые было произведено нажатие. Первый найденный в иерархии элемент будет активирован для перемещения по полотну.}
\item{\texttt{click} -- в зависимости от текущего режима, либо активирует режим перемещения камеры по полотну, либо активирует следующее событие в очереди событий презентации. В очереди могут находиться следующие события: перемещение на следующий кадр, старт либо пауза воспроизведения видео и другие. }
\item{\texttt{mouseup} -- выводит камеру из режима перемещения по полотну в режиме редактирования.}
\item{\texttt{mousemove} -- следит за перемещением мыши для определения факта перемещения камеры либо движения элемента полотна. Каждый кадр обновления запоминает последнее положение мыши и вычисляя ее смещение, производит перемещение элементов на ее основе. Вычисляет абсолютное и относительное перемещение мыши (под абсолютным подразумевается перемещение в глобальной сетке координат презентации, под относительным -- перемещение в координатной сетке камеры, то есть без учета зума камеры.)}
\end{enumerate}

\subsection{Модуль Rendering}
\label{sec:dev:rendering}

Модуль \texttt{Presently.Rendering} предоставляет набор классов и интерфейсов, объявляющих методы отрисовки объектов на полотно, реализующих логику преобразования графического контента в соответствии с положением виртуальной камеры, представленной объектом \texttt{Rendering.View-portState} и помогающих преобразовать статическое содержимое типа html в svg-элементы, пригодные для отображения на полотне.

Центральным элементом данного модуля является тип \texttt{CanvasRen-derer}, описывающий логику взаимодействия с полотном, обрабатывающий события по переотрисовке полотна и вызывающий \texttt{render} методы для типов, помеченных специальным интерфейсом \texttt{IRenderable}.

Интерфейс \texttt{IRenderable} содержит метод, определяющий порядок отрисовки элемента и дочерних ему элементов на полотне презентации. Все абстракции, объявленные в модуле \texttt{Core}, реализуют этот интерфейс, однако явным образом он используется лишь в классе \texttt{CanvasRenderer} для того, чтобы отобразить объект презентации. Сам интерфейс представлен на следующем листинге:

\begin{lstlisting}[language=TypeScript, label=lst:dev:irenderable]
export interface IRenderable {
    // Render the target object onto the canvas.
    // ctx - Context to render in
    // viewportState -- Camera info
    render(ctx: CanvasRenderingContext2D, viewportState: ViewportState): void;
}
\end{lstlisting}

Метод \texttt{render} данного интерфейса обязан реализовывать ряд операций по отрисовке компонента на полотно, используя информацию о положении камеры, переданном в viewportState, информацию о состоянии интерфейса, которую можно получить используя экземпляр \texttt{UI.AppViewMo-del}, полученный с помощью статического метода-аксессора \texttt{UI.AppView-Model.getCurrent()}

Реализация данного интерфейса изменяется от элемента к элементу, однако в основном она сводится к вызову серии методов \texttt{drawImage}, и \texttt{drawRect} контекста отображения полонта -- объекта \texttt{ctx} типа \texttt{Canvas-RenderingContext2D}.

Поскольку стандартное полотно элемента \texttt{canvas} не имеет встроенного функционала по трансформации графического материала при отрисовке аппаратно-ускоренной графики в двухмерном контексте, для реализации возможностей ортографического зума и перемещения <<поля обзора>> по полотну необходима ручная реализация данного поведения. В основе взаимодействия с элементом \texttt{canvas} лежит тип \texttt{CanvasRenderingContext2D}, инкапсулирующий набор методов по отображению изображений и геометрических примитивов на полотно, однако данные методы обладают крупным недостатком: вся логика по отображению реализована в координатном пространстве камеры (пространство, иначе называемое View Projection) без возможности использования иного, модельного пространства (Model Projection). Из-за такого ограничения становится достаточно трудно реализовать отображение элементов, чьи координаты выраженны не через пространство камеры и отображение систем элементов, где камера может перемещаться либо ортографически приближаться и отдаляться от полотна.

Основная функция, использующаяся для отрисовки содержимого - функция \texttt{ctx.drawImage}, сигнатуры которой представлены на листинге ниже:

\begin{lstlisting}[language=TypeScript, label=lst:dev:drawimg]
ctx.drawImage(image: Image,
              dx: number, dy: number): void;

ctx.drawImage(image: Image,
              dx: number, dy: number,
              dWidth, dHeight): void;

ctx.drawImage(image: Image,
              sx: number, sy: number,
              sWidth: number, sHeight: number,
              dx: number, dy: number,
              dWidth: number, dHeight: number): void;
\end{lstlisting}

Функция имеет следующие аргументы (см. рисунок \ref{fig:dev:transform}):

\begin{itemize}
  \item{\textit{image} -- изображение для отрисовки;}
  \item{\textit{dx, dy} -- координаты точки полотна, куда будет помещена левая верхняя точка изображения;}
  \item{\textit{dWidth, dHeight} -- ширина и высота области, на которое будет отрисовано изображение, позволяет изменить масштаб текстуры;}
  \item{\textit{sx, sy} -- отступ верхей левой точки, начиная с которой исходное изображение будет перенесено;}
  \item{\textit{dWidth, dHeight} -- ширина и высота фрагмента изображения, который будет отрисован на полотно, позволяет изменить масштаб текстуры;}
\end{itemize}

Комбинация этих трех методов позволяет реализовать такой набор вспомогательных функций, которые помогают добиться отрисовки изображения так, будто оно находится в трехмерном пространстве и изображение проецируется на камеру с помощью орто-преобразования. Был разработан тип, инкапсулирующий набор математических трансформаций для отображения объектов с учетом положения и относительного зума камеры.
Особого внимания заслуживает метод отрисовки координатной сетки, блок схема алгоритма которого описана на чертеже ГУИР.400201.011 ПД.

\begin{figure}[ht]
\centering
  \includegraphics[scale=1.2]{transform.png}
  \caption{Графическое представление аргументов drawImage}
  \label{fig:dev:transform}
\end{figure}

\subsection {Автоматизация сборки приложения}

Ключевое отличие приложения, реализованного на данном технологическом стеке от приложения, написанного на компилируемом языке состоит в том, что процесс сборки приложения и упаковки его в исполняемый модуль значительно отличается от компилируемого приложения. Приложения, реализованные на базе node.js и Electron выполняются в виде npm модулей, которые используют менеджер пакетов npm для установки зависимостей и библиотек-утилит.
Поскольку в проекте используется ряд надстроек над элементами стека, такие как язык TypeScript в качестве основного языка программирования, язык jade для описания разметки пользовательского интерфейса, язык less для реализации каскадных таблиц стилей, то требуется автоматизация процесса сборки приложения в дистрибутив-пакет, иначе процесс разрабоки быстро превратится в постоянный вызов компиляторов и трансляторов вместо самой разработки.

Для автоматической сборки решения используется npm-модуль \texttt{gulp}. По своей сущности \texttt{gulp} напоминает утилиту make, поставляемую с большинством дистрибутивов Linux. Для описания процесса сборки решения необходимо реализовать так называемый gulpfile -- скрипт, написанный на JavaScript и использующий gulp-расширения для произведения трансформаций над исходными файлами.

В рамках данной работы использовался gulp-модуль, автоматически отслеживающий изменения исходных файлов решения и упаковывающий транслированные их версии в дистрибутив-пакет. На листинге ниже приведен описанный скрипт:

\begin{lstlisting}[language=TypeScript, label=lst:dev:automation]
const gulp = require('gulp')
const ts = require('gulp-typescript')
const jade = require('gulp-jade')
const less = require('gulp-less')
const flatten = require('gulp-flatten')
const path = require('path')

const appConfig = require('./config.json')

gulp.task('watch', ['ts', 'less', 'jade'], () => {
    gulp.watch(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.tsScripts, '**/*.ts'), ['ts'])
    gulp.watch(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.lessStylesheets, '**/*.less'), ['less'])
    gulp.watch(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.jadeTemplates, '**/*.jade'), ['jade'])
    gulp.watch('locale.json', ['jade', 'copy-startup'])
})

gulp.task('copy-startup', () => {
    console.log('Copying startup files: ' + '{config.json,locale.json,' + path.join(appConfig.solutionSources.sourcesFolder, 'index.js') + '}')
    gulp
        .src('{config.json,locale.json,' + path.join(appConfig.solutionSources.sourcesFolder, 'index.js') + '}')
        .pipe(flatten())
        .pipe(gulp.dest(appConfig.solutionDists.distFolder))
})
\end{lstlisting}

Скрипт, приведенный выше, автоматически объявляет три задачи для отслеживателя файлов модуля gulp и ассоциирует три задачи с различными типами файлов: *.ts, *.jade и *.less. Обновление любого из перечисленных типов файлов вызывает ассоциированный тип задачи, собирающий соответствующий тип содержимого. Помимо прочьего, gulp анализирует изменение содержимого точки входа приложения и файлов конфигурации и ресурсов и автоматически копирует их в дистрибутив-пакет в случае изменения. Ниже приведен код задачи, преобразующей разметку в формате *.jade в html-файл:

\begin{lstlisting}[language=TypeScript, label=lst:dev:automation2]
gulp.task('jade', () => {
    "use strict";
    delete require.cache[require.resolve('./locale.json')]
    let localeFile = require('./locale.json')

    var locals = {
        appJs: path.join('..', appConfig.solutionDists.javascript, 'app.js'),
        appCss: path.join('..', appConfig.solutionDists.styles, 'app.css'),
        locale: localeFile
    }

    console.log('applying locale to jade templates:');
    console.log(localeFile);

    gulp
        .src(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.jadeTemplates, '**/*.jade'))
        .pipe(jade({ locals }))
        .on('error', (error) => console.log('error!\n' + error))
        .pipe(gulp.dest(path.join(appConfig.solutionDists.distFolder, appConfig.solutionDists.htmls)))
})
\end{lstlisting}

Как видно из листинга, задача не только транслирует разметку, однако применяет первичную локализацию к элементам пользовательского интерфейса: JSON-объект, находящийся в файле locale.json хранит набор строковых значений, которые ассоциированы в разметке. Транслятор Jade использует значения из JSON-объекта чтобы статически вставить строковые значения в разметку на этапе первичной компиляции. Стоит отметить, одако, что помимо статической локализации существует класс \texttt{LocaleManager} в модуле \texttt{Presently.UI}, реализующий динамический перевод сгенерированного текста на основе закешированного содержимого этого же файла locale.json.

Для загрузки содержимого JSON-файла в процессе выполнения gulp-задач используется инструкция \texttt{require} -- та же самая, которая используется для подключения стороннего или локального модуля node.js, однако предварительно производится удаление уже загруженного JSON-файла из кеша node.js. Сделано это потому что после первого выполнения инструкции \texttt{require}, node.js кеширует содержимое файлов в локальный объект \texttt{require.cache}. Удаление данного элемента позволяет загрузчику модулей повторно прочесть locale.json из файла.

Последним этапом сборки приложения после копирования всех файлов в папку dist является развертывание структуры папок в линейный список файлов. Необходимо это для более простого обращения к статическим ресурсам внутри приложения.
