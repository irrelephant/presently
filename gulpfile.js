const gulp = require('gulp')
const ts = require('gulp-typescript')
const jade = require('gulp-jade')
const less = require('gulp-less')
const flatten = require('gulp-flatten')
const path = require('path')
const winInstaller = require('electron-windows-installer');
const packageJson = require('./package.json');
const appConfig = require('./config.json')

gulp.task('ts', () => {
    gulp
        .src(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.tsScripts, '**/*.ts'))
        .pipe(ts({
            noImplicitAny: true,
            out: 'app.js',
            lib: '',
            noEmitOnError: true
        }))
        .js.pipe(gulp.dest(path.join(appConfig.solutionDists.distFolder, appConfig.solutionDists.javascript)))
})

gulp.task('jade', () => {
    "use strict";
    delete require.cache[require.resolve('./locale.json')]
    let localeFile = require('./locale.json')

    var locals = {
        appJs: path.join('..', appConfig.solutionDists.javascript, 'app.js'),
        appCss: path.join('..', appConfig.solutionDists.styles, 'app.css'),
        locale: localeFile
    }

    console.log('applying locale to jade templates:');
    console.log(localeFile);

    gulp
        .src(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.jadeTemplates, '**/*.jade'))
        .pipe(jade({ locals }))
        .on('error', (error) => console.log('error!\n' + error))
        .pipe(gulp.dest(path.join(appConfig.solutionDists.distFolder, appConfig.solutionDists.htmls)))
})

gulp.task('less', () => {
    gulp
        .src(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.lessStylesheets, '**/*.less'))
        .pipe(less({
            paths: [ path.join(__dirname, appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.lessStylesheets, appConfig.solutionSources.lessIncludes) ]
        }))
        .pipe(gulp.dest(path.join(appConfig.solutionDists.distFolder, appConfig.solutionDists.styles)))
});


gulp.task('watch', ['ts', 'less', 'jade'], () => {
    gulp.watch(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.tsScripts, '**/*.ts'), ['ts'])
    gulp.watch(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.lessStylesheets, '**/*.less'), ['less'])
    gulp.watch(path.join(appConfig.solutionSources.sourcesFolder, appConfig.solutionSources.jadeTemplates, '**/*.jade'), ['jade'])
    gulp.watch('locale.json', ['jade', 'copy-startup'])
})

gulp.task('copy-packages', function() {
  var modules = Object.keys(packageJson.dependencies);
  var moduleFiles = modules.map(function(module) {
    return 'node_modules/' + module + '/**/*';
  });
  console.log(moduleFiles);
  return gulp.src(moduleFiles, { base: 'node_modules' })
    .pipe(gulp.dest(appConfig.solutionDists.distFolder + '/node_modules'));
});

gulp.task('copy-startup', ['copy-packages'], () => {
    console.log('Copying startup files: ' + '{config.json,locale.json,' + path.join(appConfig.solutionSources.sourcesFolder, 'index.js') + '}')
    gulp
        .src('{config.json,locale.json,RELEASES,' + path.join(appConfig.solutionSources.sourcesFolder, 'index.js') + '}')
        .pipe(flatten())
        .pipe(gulp.dest(appConfig.solutionDists.distFolder))
})
