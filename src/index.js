const {app, BrowserWindow, dialog, Menu, remote} = require('electron')
const path = require('path')
const url = require('url')
const jQuery = require('jquery')

const appConfig = require('./config.json')

let win

function addSlashes(str) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

const isDebugMode = process.argv.some((arg) => {
    return arg === '-d' || arg === '--debug';
});


function setupMenu() {

    const template = [{
        label: 'File',
        submenu: [
            {role: 'reload'},
            {label: 'Open...', click() {
                var file = dialog.showOpenDialog({
                    properties: ['openFile'],
                    filters: [{ name: 'Presently Presentation (*.ly)', extensions: ['ly'] }]
                });
                if (file) win.webContents.executeJavaScript('Presently.Persistance.PersistanceManager.openPresentation("' + addSlashes(file[0]) + '")');
            }},
            {label: 'Save As...', click() {
                var file = dialog.showSaveDialog({
                    properties: ['saveFile'],
                    filters: [{ name: 'Presently Presentation (*.ly)', extensions: ['ly'] }]
                });
                if (file) win.webContents.executeJavaScript('Presently.Persistance.PersistanceManager.savePresentation("' + addSlashes(file) + '")');
            }},
            {role: 'close'},
            {role: 'minimize'},
        ]
    },
    {
        role: 'help',
        submenu: [{
            label: 'Project\'s Page',
            click () { require('electron').shell.openExternal('https://bitbucket.org/irrelephant/presently') }
        }]
    }]

    if (isDebugMode) {
        template.push({
            label: 'Debug',
            submenu: [
                {label: 'Devtools', click() {
                    win.webContents.openDevTools()
                }},
            ]
        })
    }

    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)
}

function createWindow () {

  win = new BrowserWindow({
    width: appConfig.screenSize.width,
    height: appConfig.screenSize.height
  })

  win.loadURL(url.format({
    pathname: path.join(__dirname, appConfig.solutionDists.htmls, appConfig.startupPage),
    protocol: 'file:',
    slashes: true
  }))

  if (isDebugMode) win.webContents.openDevTools()

  win.on('closed', () => {
    win = null
  })
}

app.on('ready', createWindow)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})


setupMenu()
