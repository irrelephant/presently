module Presently.Persistance {
    export class PersistanceManager {
        public static openPresentation(filePath: string) {
            PersistanceManager.ensureEmptyProjectFolder().then((projectFolder) => {
                var zip = new Zip(fs.readFileSync(filePath), { base64: false, checkCRC32: true, compression: 'DEFLATE' });

                Object.keys(zip.files).forEach((fileName: string) => {
                    let file = zip.files[fileName].asBinary();
                    fs.writeFileSync(path.join(projectFolder, fileName), file, 'binary');
                });

                let manifest = fs.readFileSync(path.join(projectFolder, 'manifest.json'));
                let persister = new PresentationPersister();
                let presentation = persister.fromJsonObject(JSON.parse(manifest));
                Core.Presentation.setCurrent(presentation);
                UI.AppViewModel.getCurrent().rebuildViewModel();
            });
        }

        private static ensureEmptyProjectFolder(): Promise<string> {
            return new Promise((resolve, reject) => {
                let presentlyAppdata = path.join(PersistanceRouter.getPreferredProjectFolder(), 'presently');
                if (!fs.existsSync(presentlyAppdata)) fs.mkdirSync(presentlyAppdata);

                let currentProject = path.join(presentlyAppdata, 'current');
                if (!fs.existsSync(currentProject)) {
                    fs.mkdirSync(currentProject);
                } else {
                    fs.readdir(currentProject, (err: any, files: any[]) => {
                        files.forEach((file: string) => {
                            fs.unlinkSync(path.join(currentProject, file));
                        });
                        resolve(currentProject);
                    })
                }
            });
        }

        private static ensureCurrentProjectFolder(): string {
            let presentlyAppdata = path.join(PersistanceRouter.getPreferredProjectFolder(), 'presently');
            if (!fs.existsSync(presentlyAppdata)) fs.mkdirSync(presentlyAppdata);

            let currentProject = path.join(presentlyAppdata, 'current');
            if (!fs.existsSync(currentProject)) fs.mkdirSync(currentProject);

            return currentProject;
        }

        private static writeManifestFile(jsonData: any): void {
            let currentProject = PersistanceManager.ensureCurrentProjectFolder();
            fs.writeFileSync(path.join(currentProject, 'manifest.json'), JSON.stringify(jsonData));
        }

        private static zipProjectFolder(targetFilePath: string) {
            let currentProject = PersistanceManager.ensureCurrentProjectFolder();
            let zip = new Zip();
            fs.readdir(currentProject, (err: any, files: any[]) => {
                files.forEach((file: string) => {
                    zip.file(file, fs.readFileSync(path.join(currentProject, file)));
                });
                var data = zip.generate({base64: false, compression: 'DEFLATE'});
                fs.writeFileSync(targetFilePath, data, 'binary');
            });
        }

        public static savePresentation(filePath: string) {
            let presentationPersister: PresentationPersister = new PresentationPersister();
            let object = presentationPersister.asJsonObject(Core.Presentation.getCurrent());
            PersistanceManager.writeManifestFile(object);
            PersistanceManager.zipProjectFolder(filePath);
        }
    }
}
