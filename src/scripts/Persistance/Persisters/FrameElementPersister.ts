module Presently.Persistance {
    export class FrameElementPersister implements IPersister<Core.FrameElement> {

        public static frameElementPersisterRegistry: {[typename: string] : any} = {};

        public asJsonObject(persistable: Core.FrameElement): any {
            let frameElementType = Core.FrameElement.getTypeName(persistable)
            let baseJson = {
                type: frameElementType,
                title: persistable.title,
                xPos: persistable.xPos,
                yPos: persistable.yPos,
                xSize: persistable.xSize,
                ySize: persistable.ySize
            };

            let childPersister = new FrameElementPersister.frameElementPersisterRegistry[frameElementType];
            return jQuery.extend(baseJson, childPersister.asJsonObject(persistable));
        }

        public fromJsonObject(jsonObject: any): Core.FrameElement {
            let persister = new FrameElementPersister.frameElementPersisterRegistry[jsonObject.type];
            return persister.fromJsonObject(jsonObject);
        }
    }
}
