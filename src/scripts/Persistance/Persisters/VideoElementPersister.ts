module Presently.Persistance {
    export class VideoElementPersister implements IPersister<Core.VideoElement> {

        public asJsonObject(persistable: Core.VideoElement): any {
            return {
                videoUri: persistable.videoUri
            };
        }

        public fromJsonObject(jsonObject: any): Core.VideoElement {
            let imageElement = new Core.VideoElement(jsonObject.xPos, jsonObject.yPos);
            return imageElement
                .setVideo(jsonObject.videoUri)
                .setSizeX(jsonObject.xSize)
                .setSizeY(jsonObject.ySize)
                .setTitle(jsonObject.title) as Core.VideoElement;
        }
    }

    // Register self in the element registry
    Presently.Persistance.FrameElementPersister.frameElementPersisterRegistry['video'] = Presently.Persistance.VideoElementPersister;
}
