module Presently.Persistance {
    export class HtmlElementPersister implements IPersister<Core.HtmlElement> {

        public asJsonObject(persistable: Core.HtmlElement): any {
            return {
                html: persistable.htmlContent
            };
        }

        public fromJsonObject(jsonObject: any): Core.HtmlElement {
            let htmlElement = new Core.HtmlElement(jsonObject.xPos, jsonObject.yPos);
            return (htmlElement
                .setTitle(jsonObject.title)
                .setSizeX(jsonObject.xSize)
                .setSizeY(jsonObject.ySize) as Core.HtmlElement)
                .setHtml(jsonObject.html);
        }
    }

    // Register self in the element registry
    Presently.Persistance.FrameElementPersister.frameElementPersisterRegistry['html'] = Presently.Persistance.HtmlElementPersister;
}
