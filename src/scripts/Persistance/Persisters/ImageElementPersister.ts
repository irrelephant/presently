module Presently.Persistance {
    export class ImageElementPersister implements IPersister<Core.ImageElement> {

        public asJsonObject(persistable: Core.ImageElement): any {
            return {
                imageUri: persistable.imageUri
            };
        }

        public fromJsonObject(jsonObject: any): Core.ImageElement {
            let imageElement = new Core.ImageElement(jsonObject.xPos, jsonObject.yPos);
            return imageElement
                .setImage(jsonObject.imageUri)
                .setSizeX(jsonObject.xSize)
                .setSizeY(jsonObject.ySize)
                .setTitle(jsonObject.title) as Core.ImageElement;
        }
    }

    // Register self in the element registry
    Presently.Persistance.FrameElementPersister.frameElementPersisterRegistry['image'] = Presently.Persistance.ImageElementPersister;
}
