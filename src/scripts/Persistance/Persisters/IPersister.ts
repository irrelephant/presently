module Presently.Persistance {
    export interface IPersister<T> {
        asJsonObject(persistable: T): any;
        fromJsonObject(jsonObject: any): T;
    }
}
