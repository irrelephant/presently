module Presently.Persistance {
    export class PresentationPersister implements IPersister<Core.Presentation> {

        public asJsonObject(persistable: Core.Presentation): any {
            let framePersister = new FramePersister();
            return {
                backgroundUri: persistable.backgroundUri,
                frames: persistable.frames.map((frame: Core.Frame) => framePersister.asJsonObject(frame))
            };
        }

        public fromJsonObject(jsonObject: any): Core.Presentation {
            let framePersister = new FramePersister();
            let presentation = new Core.Presentation();
            presentation.setBackground(jsonObject.backgroundUri);
            presentation.frames = jsonObject.frames.map((frame: any) => framePersister.fromJsonObject(frame));
            return presentation;
        }
    }
}
