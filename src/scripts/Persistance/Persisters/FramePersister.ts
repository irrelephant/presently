module Presently.Persistance {
    export class FramePersister implements IPersister<Core.Frame> {
        public asJsonObject(persistable: Core.Frame): any {
            let frameElementPersister = new FrameElementPersister();
            return {
                targetViewportState: JSON.parse(JSON.stringify(persistable.targetViewportState)),
                frameElements: persistable.frameElements.map((frameElement: Core.FrameElement) => frameElementPersister.asJsonObject(frameElement))
            };
        }

        public fromJsonObject(jsonObject: any): Core.Frame {
            let frameElementPersister = new FrameElementPersister();
            let frame = new Core.Frame(jsonObject.targetViewportState);
            frame.frameElements = jsonObject.frameElements.map((frameElement: any) => frameElementPersister.fromJsonObject(frameElement));
            return frame;
        }
    }
}
