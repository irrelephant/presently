module Presently.Persistance {
    export class PersistanceRouter {
        public static getPreferredProjectFolder() {
            return process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + 'Library/Preferences' : '/var/local');
        }
    }
}
