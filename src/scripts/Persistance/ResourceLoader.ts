/// <reference types="node" />
/// <reference types="es6-promise" />


let ytdl = require('ytdl-core');

module Presently.Persistance {
    export class ResourceLoader {
        private static prefetchVideo(src: string): Promise<string> {
            return new Promise<string>((resolve, reject) => {
                let video = ytdl(src, {filter: (format: any) => format.container === 'mp4'});
                video.pipe(fs.createWriteStream(this.getResourcePath(src) + '-tmp'));

                video.on('progress', (chunkLen: number, totalDownloaded: number, totalLen: number) => {
                    console.log(`Recieved chunk of ${chunkLen}. Progress: ${totalDownloaded}/${totalLen}`);
                });

                video.on('end', (() => {
                    console.log('done!');
                    fs.rename(this.getResourcePath(src) + '-tmp', this.getResourcePath(src), () => {
                        resolve(this.getResourcePath(src));
                    })
                }).bind(this));
            });
        }

        private static prefetchImage(src: string): Promise<string> {
            return new Promise<string>((resolve, reject) => {
                this.touchResourceFile(src);

                ((src.indexOf('https') != -1 ? https : http) as any).request(src, (response: any) => {
                    var data = new Stream();

                    response.on('data', (chunk: any) => {
                        data.push(chunk);
                    });

                    response.on('end', (() => {
                        fs.writeFile(this.getResourcePath(src), data.read(), () => {
                            resolve(this.getResourcePath(src));
                        });
                    }).bind(this));
                }).end();
            });
        }

        private static touchResourceFile(src: string): void {
            let presentlyAppdata = path.join(PersistanceRouter.getPreferredProjectFolder(), 'presently');
            if (!fs.existsSync(presentlyAppdata)) fs.mkdirSync(presentlyAppdata);

            let currentProject = path.join(presentlyAppdata, 'current');
            if (!fs.existsSync(currentProject)) fs.mkdirSync(currentProject);

            fs.writeFileSync(this.getResourcePath(src), [ 0x00 ]);
        }

        private static getResourceId(src: string): string {
            let shasum = cryptog.createHash('sha1');
            shasum.update(src.toLowerCase());
            return shasum.digest('hex');
        }

        private static getResourcePath(src: string): string {
            return path.join(PersistanceRouter.getPreferredProjectFolder(), 'presently', 'current', this.getResourceId(src));
        }

        private static doesResourceExist(src: string): boolean {
            return fs.existsSync(this.getResourcePath(src));
        }

        private static buildImageBlob(src: string): Promise<HTMLImageElement> {
            return new Promise<HTMLImageElement>((resolve, reject) => {
                fs.readFile(this.getResourcePath(src), (err: any, contents: Buffer) => {
                    if (err) reject(err);
                    let dataBlob = new Blob([contents], { type: 'image/png' });
                    let img = new Image()
                    img.src = URL.createObjectURL(dataBlob);
                    resolve(img);
                });
            });
        }

        private static buildVideoBlob(src: string): Promise<HTMLVideoElement> {
            return new Promise<HTMLVideoElement>((resolve, reject) => {
                fs.readFile(this.getResourcePath(src), (err: any, contents: Buffer) => {
                    if (err) reject(err);
                    let dataBlob = new Blob([contents], { type: 'video/mp4' });
                    let video = document.createElement('video');
                    video.src = URL.createObjectURL(dataBlob);
                    resolve(video);
                });
            });
        }

        public static getImageResource(src: string): Promise<HTMLImageElement> {
            return new Promise<HTMLImageElement>((resolve, reject) => {
                if (!this.doesResourceExist(src)) {
                    this.prefetchImage(src).then((resourcePath) => {
                        this.buildImageBlob(src).then(resolve);
                    });
                } else {
                    this.buildImageBlob(src).then(resolve);
                }
            });
        }

        public static getVideoResource(src: string): Promise<HTMLVideoElement> {
            return new Promise<HTMLVideoElement>((resolve, reject) => {
                if (!this.doesResourceExist(src)) {
                    this.prefetchVideo(src).then((resourcePath) => {
                        this.buildVideoBlob(src).then(resolve);
                    });
                } else {
                    this.buildVideoBlob(src).then(resolve);
                }
            })
        }
    }

}
