module Presently.UI {
    export class ObservableNumberField extends ObservableField<number> {
        protected transformValue(rawValue: any): number {
            return parseFloat(rawValue);
        }
    }
}
