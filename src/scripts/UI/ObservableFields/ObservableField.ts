module Presently.UI {
    export class ObservableField<T> {
        internalObservable: KnockoutObservable<T>;

        private customApply: <T>(domainObj: any, newValue: T) => void;

        protected buildBoundObservable<T>(): KnockoutObservable<T> {
            let observable: KnockoutObservable<T> = ko.observable(this.domainObject[this.propertyName]);
            observable.subscribe((newValue: T) => {
                let transformedValue: T = this.transformValue<T>(newValue);
                if (this.validate(transformedValue)) {
                    if (this.customApply) {
                        this.customApply<T>(this.domainObject, transformedValue);
                    } else {
                        this.domainObject[this.propertyName] = transformedValue;
                    }
                }
            }, null, 'change');

            return observable;
        }

        protected validate<T>(value: T): boolean {
            return true;
        }

        protected getLabel(): string {
            return LocaleManager.translate(`field_${this.propertyName}`);
        }

        protected transformValue<T>(rawValue: any): T {
            return rawValue as T;
        }

        public withCustomApply(customApply: (domainObj: any, newValue: T) => void): ObservableField<T> {
            this.customApply = customApply;
            return this;
        }

        constructor(private domainObject: any, public propertyName: string) {
            this.internalObservable = this.buildBoundObservable<T>();
        }
    }
}
