/// <reference types="jquery" />

module Presently.UI {

    export class CanvasInputManager {
        private targetCanvas: HTMLCanvasElement;
        private appViewModel: AppViewModel;
        private viewportState: ViewportState;

        private lastMouseX: number = 0;
        private lastMouseY: number = 0;
        private moving: boolean = false;

        constructor() {
            $(document).on('mousewheel', (event: any) => {
                if (this.targetCanvas == null || this.viewportState == null) return;

                let eventData = event.originalEvent;
                let centerX = this.viewportState.xPos + this.targetCanvas.width / 2 / this.viewportState.zoom;
                let centerY = this.viewportState.yPos + this.targetCanvas.height / 2 / this.viewportState.zoom;
                this.viewportState.zoom -= (eventData.deltaY / 1000);
                if (this.viewportState.zoom < 0.1) this.viewportState.zoom = 0.1;
                if (this.viewportState.zoom > 100) this.viewportState.zoom = 100;
                this.viewportState.xPos = centerX - this.targetCanvas.width / 2 / this.viewportState.zoom;
                this.viewportState.yPos = centerY - this.targetCanvas.height / 2 / this.viewportState.zoom;
            });

            $(document).on('dblclick', (event: any) => {
                let cr = Rendering.CanvasRenderer.getCurrent();
                if (cr.draggedElement != null) {
                    cr.draggedElement = null;
                } else {
                    let hitElement = cr.performHitTest(event.clientX, event.clientY);
                    cr.draggedElement = hitElement;
                }
            });

            $(document).on('mousedown', (event: any) => {
                if (this.targetCanvas == null || this.viewportState == null) return;

                let cr = Rendering.CanvasRenderer.getCurrent();
                if (cr.draggedElement && this.appViewModel.isInEditMode()) {
                    cr.draggedElement = null;
                }

                if (this.appViewModel.isInEditMode()) {
                    let eventData = event.originalEvent;
                    this.moving = true;
                    this.lastMouseX = event.screenX;
                    this.lastMouseY = event.screenY;
                } else {
                    this.appViewModel.triggerNextEvent();
                }
            });

            $(document).on('mouseup', (event: any) => {
                if (this.targetCanvas == null || this.viewportState == null) return

                if (this.appViewModel.isInEditMode()) {
                    this.moving = false;
                }
            });

            $(document).on('mousemove', (event: any) => {
                if (this.targetCanvas == null || this.viewportState == null) return;

                let eventData = event.originalEvent;
                let deltaX = eventData.screenX - this.lastMouseX;
                let deltaY = eventData.screenY - this.lastMouseY;
                this.lastMouseX = event.screenX;
                this.lastMouseY = event.screenY;

                if (this.moving && this.appViewModel.isInEditMode()) {
                    this.viewportState.xPos -= deltaX / this.viewportState.zoom;
                    this.viewportState.yPos -= deltaY / this.viewportState.zoom;
                }

                let cr = Rendering.CanvasRenderer.getCurrent();
                if (cr.draggedElement && this.appViewModel.isInEditMode()) {
                    cr.draggedElement.xPos += deltaX / this.viewportState.zoom;
                    cr.draggedElement.yPos += deltaY / this.viewportState.zoom;
                }
            });
        }

        public bind(canvas: HTMLCanvasElement, viewportState: ViewportState): void {
            this.targetCanvas = canvas;
            this.viewportState = viewportState;
        }

        public bindToAppViewModel(appModel: UI.AppViewModel): void {
            this.appViewModel = appModel;
        }
    }
}
