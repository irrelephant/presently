/// <reference types="knockout"/>

module Presently.UI {
    export class ImageElementViewModel extends FrameElementViewModel {

        protected getFieldList(): ObservableField<any>[] {
            return super.getFieldList().concat([
                new ObservableStringField(this.frameElement, 'imageUri')
                    .withCustomApply((frameElement: Core.ImageElement, newValue: string) => {
                        frameElement.setImage(newValue);
                    })
            ]);
        }
    }
}
