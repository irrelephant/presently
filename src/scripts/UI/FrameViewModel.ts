module Presently.UI
{
    export class FrameViewModel {
        frameName: KnockoutObservable<string>;
        lerpFactor: KnockoutObservable<number>;
        frameElements: KnockoutObservableArray<FrameElementViewModel>;

        constructor(public frame: Presently.Core.Frame) {
            this.frameName = ko.observable("");
            this.lerpFactor = ko.observable(frame.lerpFactor);
            this.frameElements = ko.observableArray(
                this.frame.frameElements.map((element) => element.getViewModel())
            );
        }

        public placeElement(elementTypeName: string): void {
            let elementType = Core.FrameElement.byTypeName(elementTypeName);
            if (elementType) {
                let element = new elementType(this.frame.targetViewportState.xPos, this.frame.targetViewportState.yPos, true);
                element.setSizeX(100).setSizeY(100);
                this.frame.addElement(element);
                this.frameElements.push(element.getViewModel());
            }
        }

        public reframe(): void {
            this.frame.targetViewportState = JSON.parse(JSON.stringify(Rendering.CanvasRenderer.getCurrent().viewportState));
        }

        public removeFrame(): void {
            UI.AppViewModel.getCurrent().removeFrame(this);
        }

        public moveFrameRight(): void {
            UI.AppViewModel.getCurrent().moveFrameRight(this);
        }

        public canMoveFrameRight(): boolean {
            return UI.AppViewModel.getCurrent().canMoveFrameRight(this);
        }

        public goTo(): void {
            AppViewModel.getCurrent().setCurrentAndGoToFrame(this);
        }
    }
}
