module Presently.UI {
    export class LocaleManager {
        private static localeObject: {[key: string]: string} = {};

        public static initializeLocale(localeObject: {[key: string]: string}): void {
            LocaleManager.localeObject = localeObject;
        }

        public static translate(key: string): string {
            let result = LocaleManager.localeObject[key];
            if (!result) {
                console.warn(`Locale key not found: ${key}`);
                return key;
            }
            return result;
        }
    }
}
