/// <reference types="knockout"/>

module Presently.UI {
    export class FrameElementViewModel {

        observableFields: ObservableField<any>[];

        protected getFieldList(): ObservableField<any>[] {
            return [
                new ObservableStringField(this.frameElement, 'title'),
                new ObservableNumberField(this.frameElement, 'xSize')
                    .withCustomApply((frameElement: Core.HtmlElement, newValue: number) => {
                        frameElement.setSizeX(newValue);
                    }),
                new ObservableNumberField(this.frameElement, 'ySize')
                    .withCustomApply((frameElement: Core.HtmlElement, newValue: number) => {
                        frameElement.setSizeY(newValue);
                    })
            ];
        }

        public observableFor(fieldTitle: string): KnockoutObservable<any> {
            for (let observableField of this.observableFields) {
                if (observableField.propertyName === fieldTitle) {
                    return observableField.internalObservable;
                }
            }

            return null;
        }

        public deleteElement(element: FrameElementViewModel): void {
            UI.AppViewModel.getCurrent().removeElement(this);
        }

        public moveDown(): void {
            UI.AppViewModel.getCurrent().moveElementDown(this);
        }

        public canMoveDown(index: number, elements: KnockoutObservableArray<FrameElementViewModel>): boolean {
            return index + 1 !== elements().length;
        }

        constructor(public frameElement: Core.FrameElement) {
            this.observableFields = this.getFieldList();
        }
    }
}
