/// <reference types="knockout"/>

module Presently.UI {
    export class HtmlElementViewModel extends FrameElementViewModel {

        protected getFieldList(): ObservableField<any>[] {
            return super.getFieldList().concat([
                new ObservableNoteField(this.frameElement, 'htmlContent')
                    .withCustomApply((frameElement: Core.HtmlElement, newValue: string) => {
                        frameElement.setHtml(newValue);
                    })
            ]);
        }
    }
}
