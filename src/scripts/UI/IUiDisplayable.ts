module Presently.UI {
    export interface IUiDisplayable {
        getViewModel(): any;
    }
}
