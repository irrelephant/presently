/// <reference types="knockout"/>

module Presently.UI {
    export class VideoElementViewModel extends FrameElementViewModel {

        protected getFieldList(): ObservableField<any>[] {
            return super.getFieldList().concat([
                new ObservableStringField(this.frameElement, 'videoUri')
                    .withCustomApply((frameElement: Core.VideoElement, newValue: string) => {
                        frameElement.setVideo(newValue);
                    })
            ]);
        }
    }
}
