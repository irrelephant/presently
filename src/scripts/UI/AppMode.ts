module Presently.UI {
    export class AppMode {
        public static EDIT: string = 'edit';
        public static VIEW: string = 'view';
    }
}
