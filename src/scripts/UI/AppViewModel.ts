/// <reference types="knockout"/>

module Presently.UI {

    export class AppViewModel {
        currentSelectedFrame: KnockoutObservable<FrameViewModel>;
        currentSelectedFrameElement: KnockoutObservable<FrameElementViewModel>;
        selectedElementType: KnockoutObservable<{[key: string]: string}>;
        availableElementTypes: KnockoutObservableArray<{[key: string]: string}>;
        frames: KnockoutObservableArray<FrameViewModel>;
        mode: KnockoutObservable<string>;
        backgroundUri: KnockoutObservable<string>;
        activeFrameElements: KnockoutComputed<KnockoutObservableArray<FrameElementViewModel>>;
        isAllElementsPanelVisible: KnockoutComputed<boolean>;
        currentElementPanelVisible: KnockoutComputed<boolean>;

        reevalCurrentElements: KnockoutObservable<{}>;

        private static currentAppViewModel: AppViewModel;

        public static getCurrent(): AppViewModel {
            return this.currentAppViewModel;
        }

        public static setCurrent(model: AppViewModel) : AppViewModel {
            this.currentAppViewModel = model;
            return this.currentAppViewModel;
        }

        constructor(private renderer: Rendering.CanvasRenderer, private inputManager: CanvasInputManager) {
            this.reevalCurrentElements = ko.observable();

            this.currentSelectedFrame = ko.observable(null);
            this.currentSelectedFrameElement = ko.observable(null);

            this.selectedElementType = ko.observable(null);
            this.mode = ko.observable(AppMode.EDIT);
            this.frames = ko.observableArray([]);

            this.backgroundUri = ko.observable(Core.Presentation.getCurrent().backgroundUri);
            this.availableElementTypes = ko.observableArray(
                Object.keys(Core.FrameElement.frameElementRegistry)
                    .map((key) => { return {
                        'typeName': key,
                        'displayName': LocaleManager.translate(`elementType_${key}`)
                    };
                })
            );

            this.activeFrameElements = ko.computed(() => {
                this.reevalCurrentElements();
                let frame = this.currentSelectedFrame();
                if (!frame) return;
                return frame.frameElements;
            });

            this.isAllElementsPanelVisible = ko.computed(() => {
                return !!this.isInEditMode() && !!this.currentSelectedFrame();
            });

            this.currentElementPanelVisible = ko.computed(() => {
                return !!this.isInEditMode() && !!this.currentSelectedFrameElement();
            })

            this.inputManager.bindToAppViewModel(this);
        }

        public applyPresentationMetadata(): void {
            let currentPresentation = Core.Presentation.getCurrent();
            currentPresentation.setBackground(this.backgroundUri());
        }

        public placeElement(): void {
            let elementType = this.selectedElementType()['typeName']
            this.currentSelectedFrame().placeElement(elementType);
            this.reevalCurrentElements.notifySubscribers();
        }

        public placeFrame(): void {
            let frame = Core.Presentation.getCurrent().addFrame(this.renderer.viewportState);
            this.frames.push(new FrameViewModel(frame));
            this.reevalCurrentElements.notifySubscribers();
        }

        public startPresenting(): void {
            this.mode(AppMode.VIEW);
            this.setCurrentAndGoToFrame(this.frames()[0]);
        }

        public endPresentation(): void {
            this.mode(AppMode.EDIT);
        }

        public isFrameControllerVisible(): boolean {
            return this.isInEditMode();
        }

        public canStartPresenting(): boolean {
            return this.frames().length > 0;
        }

        public isInEditMode(): boolean {
            return this.mode() === AppMode.EDIT;
        }

        public isMetadataPanelVisible(): boolean {
            return !!this.currentSelectedFrame() && this.isInEditMode();
        }

        public setCurrent(frameViewModel: FrameViewModel): void {
            this.currentSelectedFrame(frameViewModel);
        }

        public selectThisElement(element: FrameElementViewModel): void {
            this.currentSelectedFrameElement(element);
        }

        public setCurrentAndGoToFrame(frameViewModel: FrameViewModel): void {
             this.currentSelectedFrame(frameViewModel);
             this.renderer.setLerpTowardsFrame(this.currentSelectedFrame().frame);
        }

        public triggerNextEvent(): void {
            let currentFrameIndex = this.frames.indexOf(this.currentSelectedFrame());
            if (currentFrameIndex === this.frames().length - 1) {
                this.endPresentation();
            } else {
                this.setCurrentAndGoToFrame(this.frames()[currentFrameIndex + 1]);
            }
        }

        public canTriggerNextEvent(): boolean {
            let currentFrameIndex = this.frames.indexOf(this.currentSelectedFrame());
            return currentFrameIndex !== this.frames().length - 1;
        }

        public rebuildViewModel(): void {
            this.currentSelectedFrame(null)
            this.selectedElementType(null);
            this.mode(AppMode.EDIT);
            this.frames(Core.Presentation.getCurrent().frames.map((frame) => new FrameViewModel(frame)));
        }

        public removeElement(elementVm: FrameElementViewModel): void {
            for (let frame of this.frames()) {
                let elementIndex = frame.frameElements.indexOf(elementVm);
                if (elementIndex !== -1) {
                    frame.frameElements.splice(elementIndex, 1);
                    elementVm.frameElement.remove();
                }
            }
        }

        public removeFrame(frameVm: FrameViewModel): void {
            let frameIndex = this.frames.indexOf(frameVm);
            if (frameIndex !== -1) {
                this.frames.splice(frameIndex, 1);
                frameVm.frame.remove();
                this.currentSelectedFrame(null);
            }
        }

        public moveElementDown(elementVm: FrameElementViewModel): void {
            for (let frame of this.frames()) {
                let elementIndex = frame.frameElements.indexOf(elementVm);
                if (elementIndex !== frame.frameElements().length - 1) {
                    (frame.frameElements as any).swap(elementIndex, elementIndex + 1);
                    Core.Presentation.getCurrent().moveElementDown(elementVm.frameElement);
                }
            }
        }

        public moveFrameRight(frameVm: FrameViewModel): void {
            let frameIndex = this.frames.indexOf(frameVm);
            if (frameIndex !== -1 && frameIndex !== this.frames().length - 1) {
                (this.frames as any).swap(frameIndex, frameIndex + 1);
                Core.Presentation.getCurrent().moveFrameRight(frameVm.frame);
            }
        }

        public canMoveFrameRight(frameVm: FrameViewModel): boolean {
            let frameIndex = this.frames.indexOf(frameVm);
            return frameIndex !== -1 && frameIndex !== this.frames().length - 1;
        }
    }
}
