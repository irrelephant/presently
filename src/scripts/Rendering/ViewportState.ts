class ViewportState {
    public xPos: number = 0;
    public yPos: number = 0;

    public zoom: number = 1.0;
}
