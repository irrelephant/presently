module Presently.Rendering {
    export class HtmlRenderingHelper {
        public static toSvgObject(html: string, width: number, height: number): HTMLImageElement {
            let image = new Image();

            let svgData = `<svg xmlns="http://www.w3.org/2000/svg" width="${width}" height="${height}">` +
                   '<foreignObject width="100%" height="100%">' +
                     '<div xmlns="http://www.w3.org/1999/xhtml">' +
                       html +
                     '</div>' +
                   '</foreignObject>' +
                 '</svg>';

            let svgBlob = new Blob([svgData], {type: 'image/svg+xml;charset=utf-8'});
            image.src = window.URL.createObjectURL(svgBlob);
            return image;
        }
    }
}
