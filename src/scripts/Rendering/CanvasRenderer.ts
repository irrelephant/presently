/// <reference types="jquery" />

module Presently.Rendering {
    export class CanvasRenderer {
        private ctx: CanvasRenderingContext2D;
        private canvasElement: HTMLCanvasElement;

        private lerpTargetFrame: Core.Frame;

        public viewportState: ViewportState;

        public draggedElement: Core.FrameElement;

        private static currentRenderer: CanvasRenderer;

        public static getCurrent(): CanvasRenderer {
            return CanvasRenderer.currentRenderer;
        }

        public static setCurrent(renderer: CanvasRenderer): void {
            CanvasRenderer.currentRenderer = renderer;
        }

        constructor (private canvas: JQuery, private fps: number, inputManager: UI.CanvasInputManager) {
            this.canvasElement = this.canvas[0] as HTMLCanvasElement;
            this.ctx = this.canvasElement.getContext('2d');
            this.viewportState = new ViewportState();

            inputManager.bind(this.canvasElement, this.viewportState);
            setInterval(this.processFrame.bind(this), 1000 / this.fps);
        }

        private processFrame(): void {
            this.clearCanvas();
            this.resizeCanvas();
            if (this.lerpTargetFrame) {
                let lerpDone = this.lerpTargetFrame.lerpViewportStateTowards(this.viewportState);
                if (lerpDone) this.lerpTargetFrame = null;
            }
            this.render();
        }

        private resizeCanvas(): void {
            this.ctx.canvas.width  = window.innerWidth;
            this.ctx.canvas.height = window.innerHeight;
        }

        private renderBackground(): void {
            let backgroundImg = Core.Presentation.getCurrent().backgroundImage;
            if (!backgroundImg) return;

            this.ctx.drawImage(backgroundImg,
                this.viewportState.xPos,
                this.viewportState.yPos,
                backgroundImg.width / this.viewportState.zoom,
                backgroundImg.height / this.viewportState.zoom,
                0, 0, backgroundImg.width, backgroundImg.height);
        }

        private render() {
            this.renderBackground();
            Core.Presentation.getCurrent().render(this.ctx, this.viewportState);
            if (UI.AppViewModel.getCurrent().isInEditMode()) this.renderGrid();
        }

        private renderGrid() {
            if (this.viewportState.zoom < 0.5) {
                this.drawGrid(1024, 'grey', 0.8, 0.2);
            } else if (this.viewportState.zoom < 2) {
                this.drawGrid(256, 'grey', 0.3, 0.1);
            } else {
                this.drawGrid(128, 'grey', 0.2, 0.1);
            }
        }

        private drawGrid(cellSize: number, lineStyle: string, baseLineWidth: number, minLineWidth: number): void {
            let horizontal = parseInt((this.canvasElement.width / cellSize / this.viewportState.zoom).toString());
            let vertical = parseInt((this.canvasElement.height / cellSize / this.viewportState.zoom).toString());

            this.ctx.strokeStyle = lineStyle;
            this.ctx.lineWidth = baseLineWidth * this.viewportState.zoom < minLineWidth ? minLineWidth : baseLineWidth * this.viewportState.zoom;

            for (let i = -5; i < horizontal + 5; i++)
                for (let j = -5; j < vertical + 5; j++) {
                    let tlX = (cellSize - this.viewportState.xPos % cellSize);
                    let tlY = (cellSize - this.viewportState.yPos % cellSize);
                    this.ctx.strokeRect(
                        (tlX + cellSize * i) * this.viewportState.zoom,
                        (tlY + cellSize * j) * this.viewportState.zoom,
                        cellSize * this.viewportState.zoom,
                        cellSize * this.viewportState.zoom
                    );
                }
        }

        public setLerpTowardsFrame(frame: Presently.Core.Frame): void {
            this.lerpTargetFrame = frame;
        }

        private clearCanvas(): void {
            this.ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
        }

        private getAbosluteMousePosition(mouseX: number, mouseY: number): any {
            return {
                mouseX: this.viewportState.xPos + mouseX / this.viewportState.zoom,
                mouseY: this.viewportState.yPos + mouseY / this.viewportState.zoom
            };
        }

        public performHitTest(mouseX: number, mouseY: number): Core.FrameElement {
            let absoluteMouse = this.getAbosluteMousePosition(mouseX, mouseY);
            let frames = Core.Presentation.getCurrent().frames;

            for (let frame of frames) {
                for (let element of frame.frameElements) {
                    if (absoluteMouse.mouseX > element.xPos
                        && absoluteMouse.mouseX < (element.xPos + element.xSize)
                        && absoluteMouse.mouseY > element.yPos
                        && absoluteMouse.mouseY < (element.yPos + element.ySize)) {
                            return element;
                        }
                }
            }

            return null;
        }
    }
}
