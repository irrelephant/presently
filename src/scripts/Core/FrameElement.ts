module Presently.Core {
    export class FrameElement implements UI.IUiDisplayable {
        public xPos: number;
        public yPos: number;
        public xSize: number;
        public ySize: number;

        public title: string;

        public static frameElementRegistry: {[typename: string] : any} = {};

        public static byTypeName(typename: string): any {
            return FrameElement.frameElementRegistry[typename];
        }

        public static getTypeName(instance: FrameElement) {
            for (let key in FrameElement.frameElementRegistry) {
                if (instance instanceof FrameElement.frameElementRegistry[key]) return key;
            }

            return null;
        }

        constructor(xPos: number, yPos: number, useDefaultValue: boolean = false) {
            this.xPos = xPos;
            this.yPos = yPos;
            this.title = UI.LocaleManager.translate(`elementType_${FrameElement.getTypeName(this)}`);
        }

        public setPositionX(xPos: number): FrameElement {
            this.xPos = xPos;
            return this;
        }

        public setPositionY(yPos: number): FrameElement {
            this.yPos = yPos;
            return this;
        }

        public setTitle(title: string): FrameElement {
            this.title = title;
            return this;
        }

        public setSizeX(xSize: number): FrameElement {
            this.xSize = xSize;
            return this;
        }

        public setSizeY(ySize: number): FrameElement {
            this.ySize = ySize;
            return this;
        }

        protected renderOutline(ctx: CanvasRenderingContext2D, viewportState: ViewportState) {
            ctx.lineWidth = 1;
            ctx.strokeStyle = 'green';

            ctx.strokeRect((this.xPos - viewportState.xPos) * viewportState.zoom,
                           (this.yPos - viewportState.yPos) * viewportState.zoom,
                           this.xSize * viewportState.zoom,
                           this.ySize * viewportState.zoom);
        }

        public render(ctx: CanvasRenderingContext2D, viewportState: ViewportState) {
        }

        public isDragged(): boolean {
            return Rendering.CanvasRenderer.getCurrent().draggedElement === this;
        }

        public remove(): void {
            Presentation.getCurrent().removeElement(this);
        }

        // Abstract FrameElement doesn't have to yield a view model
        public getViewModel(): any {
            return null;
        }
    }
}
