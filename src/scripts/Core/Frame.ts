module Presently.Core {
    export class Frame {
        public targetViewportState: ViewportState;
        public frameElements: FrameElement[];

        public lerpFactor: number;

        constructor(currentViewport: ViewportState) {
            this.targetViewportState = JSON.parse(JSON.stringify(currentViewport));
            this.frameElements = [];
            this.lerpFactor = 0.2;
        }

        public addElement(frameElement: FrameElement) {
            this.frameElements.push(frameElement);
        }

        public setLerpFactor(factor: number) {
            this.lerpFactor = factor;
        }

        public render(ctx: CanvasRenderingContext2D, viewportState: ViewportState): void {
            if (UI.AppViewModel.getCurrent().isInEditMode()) {
                let zoomFactor = viewportState.zoom / this.targetViewportState.zoom;

                let selectedFrame = UI.AppViewModel.getCurrent().currentSelectedFrame();
                if (selectedFrame == null || selectedFrame.frame !== this) {
                    ctx.setLineDash([20, 20]);
                    ctx.lineWidth = 1;
                } else {
                    ctx.setLineDash([]);
                    ctx.lineWidth = 2;
                }

                ctx.strokeStyle = 'red';

                ctx.strokeRect((this.targetViewportState.xPos - viewportState.xPos) * viewportState.zoom,
                               (this.targetViewportState.yPos - viewportState.yPos) * viewportState.zoom,
                               ctx.canvas.width * zoomFactor,
                               ctx.canvas.height * zoomFactor);
                ctx.setLineDash([]);
            }

            this.frameElements.forEach(element => {
                element.render(ctx, viewportState);
            });
        }

        public setPositionX(xPos: number): Frame {
            this.targetViewportState.xPos = xPos;
            return this;
        }

        public setPositionY(yPos: number): Frame {
            this.targetViewportState.yPos = yPos;
            return this;
        }

        public lerpViewportStateTowards(viewportState: ViewportState): boolean {
            viewportState.xPos = viewportState.xPos + (this.targetViewportState.xPos - viewportState.xPos) * this.lerpFactor;
            viewportState.yPos = viewportState.yPos +(this.targetViewportState.yPos - viewportState.yPos) * this.lerpFactor;
            viewportState.zoom = viewportState.zoom + (this.targetViewportState.zoom - viewportState.zoom) * this.lerpFactor;

            if (Math.abs(viewportState.xPos - this.targetViewportState.xPos) < 0.1 &&
                Math.abs(viewportState.xPos - this.targetViewportState.xPos) < 0.1 &&
                Math.abs(viewportState.zoom - this.targetViewportState.zoom) < 0.01) {
                  viewportState.xPos = this.targetViewportState.xPos;
                  viewportState.yPos = this.targetViewportState.yPos;
                  viewportState.zoom = this.targetViewportState.zoom;

                  return true;
            }

            return false;
        }

        public isVisibleFromViewport(viewportState: ViewportState): boolean {
            return true;
        }

        public remove(): void {
            Presentation.getCurrent().removeFrame(this);
        }
    }
}
