module Presently.Core {
    export class Presentation {
        public frames: Frame[];
        public static current: Presentation;

        public backgroundUri: string;
        public backgroundImage: HTMLImageElement;

        constructor() {
            this.frames = [];
            this.setBackground('http://free4kwallpaper.com/wp-content/uploads/2016/01/Worm-Hole-From-Space-4K-Wallpaper.jpg');
        }

        public render(ctx: CanvasRenderingContext2D, viewportState: ViewportState): void {
            this.frames.forEach((frame) => {
                frame.render(ctx, viewportState);
            });
        }

        public setBackground(uri: string): void {
            this.backgroundUri = uri;
            Persistance.ResourceLoader.getImageResource(uri).then((image) => {
                this.backgroundImage = image;
            });
        }

        public static getCurrent(): Presentation {
            if (!Presentation.current) {
                Presentation.current = new Presentation();
            }
            return Presentation.current;
        }

        public static setCurrent(presentation: Presentation) : Presentation {
            Presentation.current = presentation;
            return presentation;
        }

        public removeElement(frameElement: FrameElement): void {
            for (let frame of this.frames) {
                let elementIndex = frame.frameElements.indexOf(frameElement);
                if (elementIndex !== -1) {
                    frame.frameElements.splice(elementIndex, 1);
                    return;
                }
            }
        }

        public moveElementDown(frameElement: FrameElement): void {
            for (let frame of this.frames) {
                let elementIndex = frame.frameElements.indexOf(frameElement);
                if (elementIndex !== -1) {
                    let tempElement = frame.frameElements[elementIndex + 1];
                    if (tempElement) {
                        frame.frameElements[elementIndex + 1] = frameElement;
                        frame.frameElements[elementIndex] = tempElement;
                    }
                    return;
                }
            }
        }

        public addFrame(currentViewport: ViewportState): Frame {
            let frame = new Frame(currentViewport);
            this.frames.push(frame);
            return frame;
        }

        public removeFrame(frame: Frame): void {
            let frameIndex = this.frames.indexOf(frame);
            if (frameIndex !== -1) {
                this.frames.splice(frameIndex, 1);
            }
        }

        public moveFrameRight(frame: Frame): void {
            let frameIndex = this.frames.indexOf(frame);
            if (frameIndex !== -1 && frameIndex != this.frames.length - 1) {
                let tmp = this.frames[frameIndex + 1];
                this.frames[frameIndex + 1] = frame;
                this.frames[frameIndex] = tmp;
            }
        }
    }
}
