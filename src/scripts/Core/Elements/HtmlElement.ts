module Presently.Core {
    export class HtmlElement extends FrameElement {

        public htmlContent: string;
        private svgContent: HTMLImageElement;

        constructor(xPos: number, yPos: number, useDefaultValue: boolean = false) {
            super(xPos, yPos);
            if (useDefaultValue) this.setHtml('<p style="color: red; border: 1px solid red;">Edit HTML contents of this frame element</p>');
        }

        public setHtml(html: string): HtmlElement {
            this.htmlContent = html;
            if (html) this.svgContent = Rendering.HtmlRenderingHelper.toSvgObject(html, this.xSize, this.ySize);
            return this;
        }

        public setSizeX(xSize: number): HtmlElement {
            this.xSize = xSize;
            this.setHtml(this.htmlContent);
            return this;
        }

        public setSizeY(ySize: number): HtmlElement {
            this.ySize = ySize;
            this.setHtml(this.htmlContent);
            return this;
        }

        public render(ctx: CanvasRenderingContext2D, viewportState: ViewportState) {
            if (UI.AppViewModel.getCurrent().isInEditMode()) {
                this.renderOutline(ctx, viewportState);
            }

            if (!this.svgContent) return;

            try {
                ctx.drawImage(this.svgContent,
                    (this.xPos - viewportState.xPos) * viewportState.zoom,
                    (this.yPos - viewportState.yPos) * viewportState.zoom,
                    this.svgContent.width * viewportState.zoom,
                    this.svgContent.height * viewportState.zoom
                );
            } catch(err) {
                console.error(err);
                this.svgContent = null;
            }            
        }

        public getViewModel(): any {
            return new UI.HtmlElementViewModel(this);
        }
    }

    // Register self in the element registry
    Presently.Core.FrameElement.frameElementRegistry['html'] = Presently.Core.HtmlElement;
}
