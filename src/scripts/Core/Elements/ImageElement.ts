module Presently.Core {
    export class ImageElement extends FrameElement {

        public imageUri: string;
        private imageHtmlContent: HTMLImageElement;

        public constructor(xPos: number, yPos: number, useDefaultValue: boolean = false) {
            super(xPos, yPos);
            if (useDefaultValue) {
                this.setImage('http://via.placeholder.com/100x100');
            }
        }

        public setImage(uri: string): ImageElement {
            this.imageUri = uri;
            Persistance.ResourceLoader.getImageResource(uri).then((image) => {
                this.imageHtmlContent = image;
            });
            return this;
        }

        public render(ctx: CanvasRenderingContext2D, viewportState: ViewportState) {
            if (UI.AppViewModel.getCurrent().isInEditMode()) {
                this.renderOutline(ctx, viewportState);
            }

            if (!this.imageHtmlContent) return;

            try {
                ctx.drawImage(this.imageHtmlContent,
                    (this.xPos - viewportState.xPos) * viewportState.zoom,
                    (this.yPos - viewportState.yPos) * viewportState.zoom,
                    this.xSize * viewportState.zoom,
                    this.ySize * viewportState.zoom
                );
            } catch (err) {
                this.imageHtmlContent = null;
                console.error(err);
            }
        }

        public getViewModel(): any {
            return new UI.ImageElementViewModel(this);
        }
    }

    // Register self in the element registry
    Presently.Core.FrameElement.frameElementRegistry['image'] = Presently.Core.ImageElement;
}
