module Presently.Core {
    export class VideoElement extends FrameElement {

        public videoUri: string;
        private videoContent: HTMLVideoElement;

        public looping: boolean;
        public playSound: boolean;

        public constructor(xPos: number, yPos: number, useDefaultValue: boolean = false) {
            super(xPos, yPos);
            if (useDefaultValue) this.setVideo('http://www.youtube.com/watch?v=A02s8omM_hI');
        }

        public setVideo(uri: string): VideoElement {
            this.videoUri = uri;
            Persistance.ResourceLoader.getVideoResource(uri).then((video) => {
                this.videoContent = video;
                this.videoContent.play();
                this.videoContent.loop = this.looping;
                this.videoContent.volume = this.playSound ? 1 : 0;
            }).catch((err) => {
                this.videoContent = null;
            });
            return this;
        }

        public setSound(enabled: boolean): VideoElement {
            this.videoContent.volume = enabled ? 1 : 0;
            this.playSound = enabled;
            return this;
        }

        public setLooping(enabled: boolean): VideoElement {
            this.videoContent.loop = enabled;
            this.looping = enabled;
            return this;
        }

        public render(ctx: CanvasRenderingContext2D, viewportState: ViewportState) {
            if (UI.AppViewModel.getCurrent().isInEditMode()) {
                this.renderOutline(ctx, viewportState);
            }

            if (!this.videoContent) return;

            try {
                ctx.drawImage(this.videoContent,
                    (this.xPos - viewportState.xPos) * viewportState.zoom,
                    (this.yPos - viewportState.yPos) * viewportState.zoom,
                    this.xSize * viewportState.zoom,
                    this.ySize * viewportState.zoom
                );
            } catch (err) {
                this.videoContent = null;
                console.error(err);
            }
        }

        public getViewModel(): any {
            return new UI.VideoElementViewModel(this);
        }
    }

    // Register self in the element registry
    Presently.Core.FrameElement.frameElementRegistry['video'] = Presently.Core.VideoElement;
}
