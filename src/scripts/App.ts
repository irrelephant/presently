/// <reference types="jquery" />
/// <reference types="knockout"/>

// Global window configuration
let appConfig: any;
let locale: any;

let http = require('http');
let https = require('https');
let stream = require('stream');
let Stream = stream.Transform;

let fs = require('fs');
let url = require('url');
let path = require('path');
let cryptog = require('crypto');

let Zip = require('node-zip');

module Presently {
    let prepareCanvas = (canvas: JQuery): void => {
        canvas.attr('width', appConfig.screenSize.width);
        canvas.attr('height', appConfig.screenSize.height);
    }

    let loadingCheker: number;

    let loadingDone = () => {
        return UI && UI.LocaleManager && UI.AppViewModel && Rendering
        && Rendering.CanvasRenderer && Persistance
        && Persistance.PersistanceManager && Persistance.PersistanceRouter && Persistance.ResourceLoader;
    }

    (ko.bindingHandlers as any).singleOrDoubleClick = {
        init: (element: JQuery, valueAccessor: any, allBindingsAccessor: any, viewModel: any, bindingContext: any) => {
            let singleHandler: any = valueAccessor().click;
            let doubleHandler: any = valueAccessor().dblclick;
            let delay: number = valueAccessor().delay || 200;
            let clicks: number = 0;

            $(element).click((event: JQueryEventObject) => {
                clicks++;
                if (clicks === 1) {
                    setTimeout(() => {
                        if(clicks === 1) {
                            if (singleHandler !== undefined) {
                                singleHandler.call(viewModel, bindingContext.$data, event);
                            }
                        } else {
                            if (doubleHandler !== undefined) {
                                doubleHandler.call(viewModel, bindingContext.$data, event);
                            }
                        }
                        clicks = 0;
                    }, delay);
                }
            });
        }
    };

    (ko.observableArray.fn as any).swap = function(index1: number, index2: number) {
        this.valueWillMutate();

        var temp = this()[index1];
        this()[index1] = this()[index2];
        this()[index2] = temp;

        this.valueHasMutated();
    }

    jQuery(document).ready(() => {
        loadingCheker = setInterval(() => {
            if (!loadingDone()) return;
            clearInterval(loadingCheker);
            UI.LocaleManager.initializeLocale(locale);

            let canvas = $('#main-canvas');
            prepareCanvas(canvas);

            let inputManager = new Presently.UI.CanvasInputManager();

            let canvasRenderer = new Presently.Rendering.CanvasRenderer(canvas, 60, inputManager);
            Presently.Rendering.CanvasRenderer.setCurrent(canvasRenderer);

            let appViewModel = new Presently.UI.AppViewModel(canvasRenderer, inputManager);
            Presently.UI.AppViewModel.setCurrent(appViewModel);

            ko.applyBindings(Presently.UI.AppViewModel.getCurrent(), jQuery('#ui-container')[0]);
        });
    });
}
